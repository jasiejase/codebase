<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CorporationController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\OrderPageController;
use App\Http\Controllers\OrderPageNoteController;
use App\Http\Controllers\OrderItemController;
use App\Http\Controllers\OrderItemHistoryController;
use App\Http\Controllers\OrderItemTranslationController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\QuoteController;
use App\Http\Controllers\ThresholdController;
use App\Http\Controllers\ThresholdHistoryController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example Routes
Route::view('/', 'auth.login');
Route::match(['get', 'post'], '/dashboard', function(){
    return view('dashboard');
});
Route::view('/pages/slick', 'pages.slick');
Route::view('/pages/datatables', 'pages.datatables');
Route::view('/pages/blank', 'pages.blank');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/markAsRead', [UserController::class, 'markAsReadNotifications']);

// User Routes;
Route::get('/user', [UserController::class, 'index'])->name('user.index');
Route::post('/user/show', [UserController::class, 'show'])->name('user.show');
Route::post('/user/update', [UserController::class, 'update'])->name('user.update');

// Profile Routes
Route::get('/profile', [ProfileController::class, 'index'])->name('profile.index');
Route::post('/profile/update', [ProfileController::class, 'update'])->name('profile.update');
Route::post('/profile/updatePassword', [ProfileController::class, 'updatePassword'])->name('profile.updatePassword');

// Company Routes
Route::get('/company', [CompanyController::class, 'index'])->name('company.index');
Route::post('/company/show', [CompanyController::class, 'show'])->name('company.show');
Route::post('/company/create', [CompanyController::class, 'create'])->name('company.create');
Route::post('/company/update', [CompanyController::class, 'update'])->name('company.update');

// Corporation Routes
Route::get('/corporation', [CorporationController::class, 'index'])->name('corporation.index');
Route::post('/corporation/show', [CorporationController::class, 'show'])->name('corporation.show');
Route::post('/corporation/update', [CorporationController::class, 'update'])->name('corporation.update');

// Contact Routes
Route::post('/contact/index', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact/show', [ContactController::class, 'show'])->name('contact.show');
Route::post('/contact/edit', [ContactController::class, 'edit'])->name('contact.edit');

// Project Routes
Route::get('/project', [ProjectController::class, 'index'])->name('project.index');
Route::post('/project/getCompanyInfo', [ProjectController::class, 'getCompanyInfo'])->name('project.getCompanyInfo');
Route::post('/project/getProjectList', [ProjectController::class, 'getProjectList'])->name('project.getProjectList');
Route::post('/project/getProjectInfo', [ProjectController::class, 'getProjectInfo'])->name('project.getProjectInfo');
Route::post('/project/editProject', [ProjectController::class, 'editProject'])->name('project.editProject');

// OrderPage Routes
Route::post('/orderPage/show', [OrderPageController::class, 'show'])->name('orderPage.show');
Route::post('/orderPage/edit', [OrderPageController::class, 'edit'])->name('orderPage.edit');

// OrderPageNote Routes
Route::post('/orderPageNote', [OrderPageNoteController::class, 'index'])->name('orderPageNote.index');
Route::post('/orderPageNote/create', [OrderPageNoteController::class, 'create'])->name('orderPageNote.create');
Route::get('/orderPageNote/download/{id}', [OrderPageNoteController::class, 'download'])->name('orderPageNote.download');

// OrderItem Routes 南亞 orders
Route::post('/orderItem', [OrderItemController::class, 'index'])->name('orderItem.index');
Route::post('/orderItem/update', [OrderItemController::class, 'update'])->name('orderItem.update');

// OrderItem Routes 南亞 orders
Route::post('/orderItemTranslation', [OrderItemTranslationController::class, 'index'])->name('orderItemTranslation.index');

// OrderItemHistory Routes
Route::post('/orderItemHistory', [OrderItemHistoryController::class, 'index'])->name('orderItemHistory.index');
Route::post('/orderItemHistory/shippedHistory', [OrderItemHistoryController::class, 'shippedHistory'])->name('orderItemHistory.shippedHistory');

// Threshold Routes 門檻 orders
Route::post('/threshold', [ThresholdController::class, 'index'])->name('threshold.index');
Route::post('/threshold/update', [ThresholdController::class, 'update'])->name('threshold.update');

// ThresholdHistory Routes
Route::post('/thresholdHistory', [ThresholdHistoryController::class, 'index'])->name('thresholdHistory.index');
Route::post('/thresholdHistory/shippedHistory', [ThresholdHistoryController::class, 'shippedHistory'])->name('thresholdHistory.shippedHistory');

// Supplier Routes
Route::get('/supplier', [SupplierController::class, 'index'])->name('supplier.index');
Route::post('/supplier/show', [SupplierController::class, 'show'])->name('supplier.show');
Route::post('/supplier/create', [SupplierController::class, 'create'])->name('supplier.create');
Route::post('/supplier/update', [SupplierController::class, 'update'])->name('supplier.update');
Route::post('/supplier/viewMaterial', [SupplierController::class, 'viewMaterial'])->name('supplier.viewMaterial');
Route::post('/supplier/viewShipment', [SupplierController::class, 'viewShipment'])->name('supplier.viewShipment');

// Seller Routes
Route::post('/seller/index', [SellerController::class, 'index'])->name('seller.index');
Route::post('/seller/show', [SellerController::class, 'show'])->name('seller.show');
Route::post('/seller/edit', [SellerController::class, 'edit'])->name('seller.edit');

// Supplier Routes
Route::get('/material', [MaterialController::class, 'index'])->name('material.index');
Route::post('/material/show', [MaterialController::class, 'show'])->name('material.show');
Route::post('/material/create', [MaterialController::class, 'create'])->name('material.create');
Route::post('/material/update', [MaterialController::class, 'update'])->name('material.update');
Route::post('/material/viewMaterial', [MaterialController::class, 'viewMaterial'])->name('material.viewMaterial');
Route::post('/material/viewShipment', [MaterialController::class, 'viewShipment'])->name('material.viewShipment');

// Shipments
Route::get('/shipment', [ShipmentController::class, 'index'])->name('shipment.index');

// Quotes
Route::post('/quote/show', [QuoteController::class, 'show'])->name('quote.show');
