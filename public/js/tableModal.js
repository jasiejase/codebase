// This class is used in conjuction with bootstrap-table-wrapper.js
// It grabs the form data of the modal for that specific table

class TableModal {
    constructor(tableId, modalData) {
        this.tableId = tableId;
        this.modalData = modalData;
    }

    // Show Table Modal
    show(row, index) {
        const $title = $(`#modal-${this.tableId} .modal-name`);

        // Check if adding new row or updating existing row
        // then change title and disable accordingly
        if (index < 0) {
            $title.html(this.modalData.newName);
            this.modalData.disabled.forEach((selector, i) => {
                $(`#${this.tableId}-${selector}`).removeAttr('disabled');
            });

        } else {
            $title.html(row[this.modalData.name]);
            this.modalData.disabled.forEach((selector, i) => {
                $(`#${this.tableId}-${selector}`).prop('disabled', 'true');
            });
        }

        // Update form value
        this.modalData.id.forEach((selector, i) => {
            $(`#${this.tableId}-${selector}`).val(row[selector]).trigger('change');
        });
        $(`#${this.tableId}-index`).val(index);

        // Reset any error message
        $('.form-control.error').removeClass('error');
        $('.error').remove();

        // Show Modal
        $(`#modal-${this.tableId}`).modal('show');
    }

    update(BTableWrapper, addUrl, updateUrl) {
        const form = $(`#form-${this.tableId}`);
        form.validate();

        if (form.valid()) {
            let message = "";
            let index = "";
            let data = {};
            const multiple = this.modalData.multiple;
            const main = this.modalData.main;
            const modalId = 'modal-' + this.tableId;


            // Get form data
            this.modalData.id.forEach((selector, i) => {
                data[selector] = $(`#${this.tableId}-${selector}`).val();
            });
            index = data.index;

            // Add/Update data to database and table
            if (!(index < 0)) {
                // Update
                $.post(updateUrl, data,  (result) => {
                    for (const [key, value] of Object.entries(this.modalData.multiple)) {
                        data[value] = $(`#${this.tableId}-${key} option:selected`).toArray().map(item => item.text).join("<br>");
                    }

                    BTableWrapper.updateRow(index, data);
                    $(`#modal-${this.tableId}`).modal('hide');

                    message = data[this.modalData.main] + "的資料修改成功!";
                    notify(message, 'fa fa-info');
                });
            } else {
                // Add
                $.post(addUrl, data,  (result) => {
                    for (const [key, value] of Object.entries(this.modalData.multiple)) {
                        const $multiple = $(`#${this.tableId}-${key}`);
                        let array = $multiple.val();
                        array.push('' + result[key + '_id']);

                        $multiple.append(new Option(result[key + '_name'], result[key + '_id']));

                        let texts = [];
                        $(`#${this.tableId}-${key} option`).each(function () {
                            if (array.includes($(this).val())) {
                                texts.push($(this).html());
                            }
                        });
                        data[value] = texts.join("<br>");
                    }

                    data["id"] = result.id;
                    BTableWrapper.addRow(data);
                    $(`#modal-${this.tableId}`).modal('hide');

                    message = "成功新增客戶 " + $(`#${this.tableId}-${this.modalData.main}`).val();
                    this.notify(message, 'fa fa-info');
                });
            }
        }
    }

    notify(msg, icon, type='success', element='body') {
        $.notify({
            icon: icon,
            message: msg,
        }, {
            placement: {
                from: "top",
                align: "center"
            },
            type: type,
            element: element
        });
    }
}
