/*************************************************************
 *  Project related
 *************************************************************/

$('#company, #project').select2();
$('.content .col-md-6:first-child .select2-container:first-of-type').css('width', '100%');
$('#project').next('.select2-container').css('width', 'calc(100% - 100.7px)');

const getCompanyInfoUrl = 'project/getCompanyInfo';
const getProjectListUrl = 'project/getProjectList';
const getProjectInfoUrl = 'project/getProjectInfo';
const editProjectUrl    = 'project/editProject';

// Used onchange select customer
$('#company').on('change', function() {
    var id = $(this).val();
    var name = $('#company option:selected').text();

    getCompanyInfo(id, name);
    getProjectList(id);

    $('#project').trigger('change');
});

// get company info
function getCompanyInfo(id, company) {
    var $title   = $('#company-info .block-title');
    var $content = $('#company-info .block-content');
    var $contact = $('#project-contacts');

    if (id != "") {
        $.post(getCompanyInfoUrl, {id: id}, function (data) {
        var phone = data.company.phone;
        if (data.company.phone2 != "") {
            phone = phone + ", " + data.company.phone2;
        }

        $title.html(company);
        $content.find('table tr:first-child td:last-child').html(data.company.gui);
        $content.find('table tr:nth-child(2) td:last-child').html(data.company.address);
        $content.find('table tr:nth-child(3) td:last-child').html(phone);
        $content.find('table tr:nth-child(4) td:last-child').html(data.company.fax);
        $content.find('table tr:last-child td:last-child').html("");

        showContact($content.find('table tr:last-child td:last-child'), data.company.contacts, 'warning');
        showContact($content.find('table tr:last-child td:last-child'), data.extra, 'secondary');

        //Fill in contact options in project modal
        $contact.empty();

        data.company.contacts.forEach((info, index) => {
            $contact.append('<option value="' + info.id + '">' + info.name + '</option>');
        });
        data.extra.forEach((info, index) => {
            $contact.append('<option value="' + info.id + '">' + info.name + '</option>');
        });

        });

    } else {
        $title.html("客戶資料");
        $content.find("table tr td:last-child").each(function () {
            $(this).html("");
        });

        // Clear contact optiones in project modal
        $contact.empty();
    }
}

function showContact(selector, list, style) {
    list.forEach((info, index) => {
        var detail = "<button type='button' class='btn btn-alt-" + style + " js-tooltip-enabled m-1' " +
                    "data-html='true' data-toggle='tooltip' data-placement='top' title='電話:" + info.phone + "<br>手機:" + info.mobile + "<br>Email:" + info.email +"'>" + info.name + "</button>";
        selector.append(detail);
    });
    $('[data-toggle="tooltip"]').tooltip();
}

// Get Project List
function getProjectList(id) {
    $('#project').html("<option value=''> --- </option>");
    $('#new-project, #project').prop('disabled', 'true');
    $('#project').prop('disabled', 'true');

    if (id != "") {
        $.post(getProjectListUrl, {id: id}, function (data) {
        data.projects.forEach((info, index) => {
            var option = "<option value='" + info.id + "'>" + info.site + "</option>";
            $('#project').append(option);
        });
        $('#new-project, #project').removeAttr('disabled');
        });
    }
}

$('#project').on('change', function () {
    var id = $(this).val();
    var name = $('#project option:selected').text();
    getProjectInfo(id, name);
});

function getProjectInfo(id, project) {
    var $button  = $('#new-project');
    var $title   = $('#project-info .block-title');
    var $content = $('#project-info .block-content');
    var $projectModal = $('#modal-project');

    $projectModal.find('#project-company_id').val(id);

    if (id != "") {
        $button.html('<i class="fa fa-edit"></i> 修改');

        $.post(getProjectInfoUrl, {id: id}, function (data) {
        var contact = data.project.site_contact + " / " + data.project.site_phone;

        $title.html(project);
        $content.find('table tr:first-child td:last-child').html(data.project.address);
        $content.find('table tr:nth-child(2) td:last-child').html(contact);
        $content.find('table tr:nth-child(3) td:last-child').html("");
        $content.find('table tr:last-child td:last-child').html(data.project.note);

        showContact($content.find('table tr:nth-child(3) td:last-child'), data.project.contacts, 'info');

        // Fill in project modal
        $projectModal.find('.modal-name').html(data.project.site);
        $projectModal.find('#project-site').val(data.project.site);
        $projectModal.find('#project-address').val(data.project.address);
        $projectModal.find('#project-site_contact').val(data.project.site_contact);
        $projectModal.find('#project-site_phone').val(data.project.site_phone);
        $projectModal.find('#project-note').val(data.project.note);

        var list = [];
        data.project.contacts.forEach((info, index) => {
            list.push(info.id);
        });
        $projectModal.find('#project-contacts').val(list).trigger('change');
        });

    } else {
        $button.html('<i class="fa fa-plus"></i> 新增建案');
        $title.html("建案資料");
        $content.find("table tr td:last-child").each(function () {
        $(this).html("");
        });
        $projectModal.find('.modal-name').html('新建案');
        $projectModal.find('input[type="text"]').val("");
        $('#project-contacts').val('').trigger('change');
    }
}

function editProject() {
    $('#form-project').validate({
        submitHandler: function () {
        var data = {
                    id : $('#project').val(),
                    company_id: $('#company').val(),
                    site: $('#project-site').val(),
                    address: $('#project-address').val(),
                    site_contact: $('#project-site_contact').val(),
                    site_phone: $('#project-site_phone').val(),
                    contacts: $('#project-contacts').val(),
                    note: $('#project-note').val()
        }

        $.post(editProjectUrl, data, function (result) {
            var message = "";
            var icon = "";

            if (data.id != "") {
                message = "成功修改建案: <strong>" + data.site + "</strong>";
                icon = 'fa fa-truck';
            } else {
                message =  "新增加建案: <strong>" + data.site + "</strong>";
                icon = "fa fa-truck";
                $('#project').prepend('<option value="' + result.id + '">' + data.site + '</option>');
            }

            $('#project').val(result.id).trigger('change');
            $('#modal-project').modal('hide');
            notify(message, icon, 'success', 'body');
        });
        return false;
        }
    });
}
