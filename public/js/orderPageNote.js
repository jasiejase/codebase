// Display Note
function displayOrderPageNote(id) {
    $('#newOrderPageForm button').removeAttr('disabled');
    $.post('/orderPageNote', {id: id}, function (data) {
        for (i=0; i<data.length; i++) {
            var item = data[i];
            displayOneNote(item.created_at, item.user.name, item.note, item.order_page_note_files);
        }
    });
}

function displayOneNote(tdate, user, note, files) {
    var html = '<div class="ml-20 mr-20 border border-primary rounded">' +
               '  <div class="block-header p-2 bg-gray">' + moment(tdate).format('YYYY-MM-DD hh:mma') + ' ' + user + ' 說:</div>' +
               '  <div class="block-content pt-5">'+ note.replaceAll('\r\n', '<br>');

    if (files.length > 0) {
        html += '    <hr>';
    }

    var lists = files.map(function (item, key) {
                    return '<a href="/orderPageNote/download/' + item.id + '">' + item.name + '</a>'
                });

    html += lists.join("<br>") + '</div></div><hr>';

    $('#orderPageNotes').append(html);
}

// Add new note
$('#newOrderPageForm').on('submit', function(e) {
    e.preventDefault();

    var fd = new FormData(this);
    fd.append('id', $('#orderItem-order_page_id').val());

    $.ajax({
        url: '/orderPageNote/create',
        type: 'post',
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            displayOneNote(data.time, data.user, data.note, data.paths);
            $('#newOrderPageForm textarea, #newOrderPageForm input').val("");
        }
    });
});

function clearOrderPageNote() {
    $('#orderPageNotes').html("");
    $('#newOrderPageForm textarea, #newOrderPageForm input').val("");
    $('#newOrderPageForm button').prop('disabled', 'true');
}
