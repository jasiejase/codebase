class BTableWrapper {
    constructor(tableId, ajaxUrl, ajaxData, buttons, selectedRows) {
        this.tableId = tableId;
        this.table = $(`#${tableId}`);
        this.toolbar = '#toolbar-' + tableId;
        this.ajaxUrl = ajaxUrl;
        this.ajaxData = ajaxData;
        this.buttons = buttons;
        this.events = {};
        this.buttonColumn = {};
        this.selectedRows = selectedRows;
        this.fixedCol = 2;
        this.fixedColRight = 1;
        this.showFooter = true;
        this.theadClasses = 'thead-blue'
    }

    createTable() {
        if ($.fn.bootstrapTable) {
            this.table.bootstrapTable('destroy');
        }

        $.post(this.ajaxUrl, this.ajaxData, (response) => {
            let header = response.header;
            let rows = response.row;
            let ignore = [];

            // Add column buttons
            const operateIndex = header.findIndex(column => column.field == 'operate');
            response.header[operateIndex].events = this.events;
            response.header[operateIndex].formatter = this.buttonColumn;

            // Ignore List for print & export method
            header.forEach(column => {
                if (column.hasOwnProperty("printIgnore") && column.printIgnore) {
                    ignore.push(column.field);
                }
            });

            this.table.bootstrapTable({
                columns: header,
                data: rows,
                locale: 'zh-TW',
                theadClasses: this.theadClasses,
                exportDataType: 'all',
                exportTypes: ['json', 'csv', 'txt'],
                fixedColumns: true,
                fixedNumber: this.fixedCol,
                fixedRightNumber: this.fixedColRight,
                toolbar: this.toolbar,
                showFooter: this.showFooter,
                search: true,
                visibleSearch: true,
                showSearchClearButton: true,
                showToggle: true,
                showFullscreen: true,
                showExport: true,
                showPrint: true,
                sortable: true,
                pagination: true,
                pageSize: 10,
                clickToSelect: true,
                checkboxHeader: true,
                exportOptions: {
                    ignoreColumn: ignore
                }
            });

            this.table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', () => {
                this.selectedRows = this.table.find('tr.selected').map(function () {
                    return $(this).data('index');
                }).get();
            });
        });
    }

    // Need to use this before creating table
    addUserButtons() {
        const events = {};
        const buttonColumn = this.buttons.map(button => {
            const { name, icon, callback } = button;
            events[`click .${name}`] = callback;

            return `<a class="${name}" href="javascript:void(0)" title="${name}">
                     <i class="fa fa-${icon}"></i>
                     </a> `;
        }).join('');
        this.events = events;
        this.buttonColumn = buttonColumn;
    }

    // Only use the below methods AFTER creating table

    // Append new row to the last row
    addRow(row) {
        this.table.bootstrapTable('append', row);
        this.table.bootstrapTable('scrollTo', 'bottom');
    }

    // Update row data
    updateRow(rowId, row) {
        console.log(rowId);
        console.log(row);
        $('#table').bootstrapTable('updateRow', {
            index: rowId,
            row: row
        });
    }

    // delete row data
    deleteRow(rowId) {
        this.table.bootstrapTable('remove', {
            field: 'id',
            values: rowId
        });
    }

    // Get selected rows <-- currently not sure if this is neededa or if this work if put it in the class
    getSelected() {
        return this.selectedRows;
    }
}
