class BTable {

    constructor(tableId, header, data) {
        this.tableId = tableId;
        this.selector = $('#' + tableId);
        this.toolbar = '#toolbar-' + tableId;
        this.fixedCol = 2;
        this.fixedColRight = 1;
        this.showFooter = true;
        this.theadClasses = 'thead-blue',
        this.header = header;
        this.data = data;
    }

    create() {
        this.selector.bootstrapTable('destroy').bootstrapTable({
            columns: this.header,
            data: this.data,
            locale: 'zh-TW',
            theadClasses: this.theadClasses,
            exportDataType: 'all',
            exportTypes: ['json', 'csv', 'txt'],
            fixedColumns: true,
            fixedNumber: this.fixedCol,
            fixedRightNumber: this.fixedColRight,
            toolbar: this.toolbar,
            showFooter: this.showFooter,
            search: true,
            visibleSearch: true,
            showSearchClearButton: true,
            showToggle: true,
            showFullscreen: true,
            showExport: true,
            showPrint: true,
            sortable: true,
            pagination: true,
            pageSize: 10,
            clickToSelect: true,
        }).bootstrapTable('hideColumn', 'id').bootstrapTable('hideColumn', 'material_id');

    }

    setTheadClass(colour) {
        this.theadClasses = 'thead-' + colour;
    }

    setFixedColumn(digit) {
        this.fixedCol = digit;
    }

    setFixedColumnRight(digit) {
        this.fixedColRight = digit;
    }

    setShowFooter(decision) {
        this.showFooter = decision;
    }
}

function selectedRow(table) {
    var rows = [];
    rows = $.map(table.bootstrapTable('getSelections'), function (row) {
        return row.id;
    });
    return rows;
}

function setTotalText() {
    return "總數:";
}

function getColumnCount(data) {
    return data.length;
}

function getColumnSum(data) {
    var field = this.field;
    return '<span class="text-dark">' + data.map(function (row) { return + row[field] })
               .reduce(function (sum, i) { return sum + i }, 0) + '</span>';
}

function showOperateColumn(value, row, index) {
    let buttons = [];
    $modalIds.rowButtons.forEach((buttonType, i) => {
        if (buttonType == "edit") {
            buttons.push('<a class="edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>');
        } else if (buttonType == "contact") {
            buttons.push('<a class="contact" href="javascript:void(0)" title="Contact"><i class="fa fa-address-book"></i></a>');
        } else if (buttonType == "seller") {
            buttons.push('<a class="seller" href="javascript:void(0)" title="Seller"><i class="fa fa-address-book"></i></a>');
        } else if (buttonType == "view") {
            buttons.push('<a class="view" href="javascript:void(0)" title="View"><i class="fa fa-eye"></i></a>');
        } else if (buttonType == "viewMaterial") {
            buttons.push('<a class="view-material" href="javascript:void(0)" title="檢視歷史報價"><i class="fa fa-eye"></i></a>');
        }
    });

    return buttons.join('&nbsp;&nbsp;&nbsp;');
}

window.operateEvents = {
    'click .edit': function (el, id, row, index) {
        var tableId = this.$body.prevObject[0].id;
        showTableModal(tableId, row, index, $modalIds);
    },
    'click .contact': function (el, id, row, index) {
        $.post('/contact/index', {id: row.id}, function (result) {
            var $contactList = $('#contact_list');
            $contactList.html("");
            $contactList.append('<a href="javascript:selectContact(0, 1)"><li class="list-group-item"><i class="fa fa-plus" /> 新增聯絡人</li></a>');
            result.contacts.forEach((item, index) => {
                $contactList.append('<a href="javascript:selectContact(' + item.id + ', ' + (index+2)  + ')"><li class="list-group-item"><i class="fa fa-user" /> ' + item.name + '</li></a>');
            });
            $('.modal-name').html(row.company);
            $('#company_id').val(row.id);
            $('#contact_list a:first-child li').trigger('click');

            $('#modal-contact').modal('show');
        })
    },
    'click .seller': function (el, id, row, index) {
        $.post('/seller/index', {id: row.id}, function (result) {
            var $sellerList = $('#seller_list');
            $sellerList.html("");
            $sellerList.append('<a href="javascript:selectSeller(0, 1)"><li class="list-group-item"><i class="fa fa-plus" /> 新增聯絡人</li></a>');
            result.sellers.forEach((item, index) => {
                $sellerList.append('<a href="javascript:selectSeller(' + item.id + ', ' + (index+2)  + ')"><li class="list-group-item"><i class="fa fa-user" /> ' + item.name + '</li></a>');
            });
            $('.modal-name').html(row.company);
            $('#supplier_id').val(row.id);
            $('#seller_list a:first-child li').trigger('click');

            $('#modal-seller').modal('show');
        })
    },
    'click .view': function (el, id, row, index) {
        viewOrderItems(row); // Used in "/project"
    },
    'click .editItem': function (el, id, row, index) {
        editItem(row); // Used in "/project
    },
    'click .svgItem': function (el, id, row, index) {
        viewItemSvg(row); // Used in "/project"
    },
    'click .view-material': function (el, id, row, index) {
        viewMaterial(row); // Used in "/supplier"
        viewShipment(row); // Used in /supplier
    }
}

function addBTableRow(tableId, data) {
    $('#' + tableId).bootstrapTable('append', data);
    $('#' + tableId).bootstrapTable('scrollTo', 'bottom');
}

function updateBTableRow(tableId, data, index) {
    $('#' + tableId).bootstrapTable('updateRow', {
        index: index,
        row: data
    });
}

function removeBTableRow(tableId, ids) {
    $('#' + tableId).bootstrapTable('remove', {
        field: 'id',
        values: ids
    });
}

function createBTable(tableId, route, sRow, showFooter, postData={}) {
    $.post(route, postData, function (data) {
        var $table = $('#' + tableId);
        var userTable = new BTable(tableId, data.header, data.row);
        userTable.setShowFooter(showFooter);
        userTable.create();

        $table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
            sRow = selectedRow($table);
        });
    });
}

function showTableModal(tableId, row, index, modalIds) {
    console.log(row);
    var $title = $('#modal-' + tableId + ' .modal-name');
    $title.html(row[modalIds.name]);
    if (row[modalIds.name] == null) $title.html($modalIds.newName);

    modalIds.disabled.forEach((selector, i) => {
        if (index < 0) {
            $('#' + tableId + '-' + selector).removeAttr('disabled');
        } else {
            $('#' + tableId + '-' + selector).prop('disabled', 'true');
        }
    });

    modalIds.id.forEach((selector, i) => {
        $('#' + tableId + '-' + selector).val(row[selector]).trigger('change');
    });
    $('#' + tableId + '-index').val(index);

    $('.form-control.error').removeClass('error');
    $('.error').remove();
    $('#modal-' + tableId).modal('show');
}

function editBTableRow(tableId, modalIds, addUrl, updateUrl) {
    $('#form-' + tableId).validate({
        submitHandler: function (form) {
            var message = "";
            var index = '';
            var data = {};

            modalIds.id.forEach((selector, i) => {
                if (selector == "index") {
                    index = $('#' + tableId + '-index').val();
                    return;
                }
                data[selector] = $('#' + tableId + '-' + selector).val();
            });

            if (!(index < 0)) {
                // edit
                $.post(updateUrl, data, function (result) {
                    for (const [key, value] of Object.entries(modalIds.multiple)) {
                        data[value] = $('#' + tableId + '-' + key + ' option:selected').toArray().map(item => item.text).join("<br>");
                    }

                    updateBTableRow(tableId, data, index);
                    $('#modal-' + tableId).modal('hide');

                    message = $('#' + tableId + '-' + $modalIds.main).val() + "的資料修改成功!";
                    notify(message, 'fa fa-info');
                });
            } else {
                //create
                $.post(addUrl, data, function (result) {
                    for (const [key, value] of Object.entries(modalIds.multiple)) {
                        var $multiple = $('#' + tableId + '-' + key);
                        var array = $multiple.val();
                        array.push('' + result[key + '_id']);

                        $multiple.append(new Option(result[key + '_name'], result[key + '_id']));

                        var texts = [];
                        $('#' + tableId + '-' + key + ' option').each(function () {
                            if (array.includes($(this).val())) {
                                texts.push($(this).html());
                            }
                        });
                        data[value] = texts.join("<br>");
                    }

                    data["id"] = result.id;
                    addBTableRow(tableId, data);
                    $('#modal-' + tableId).modal('hide');

                    message = "成功新增客戶 " + $('#' + tableId + '-' + $modalIds.main).val();
                    notify(message, 'fa fa-info');
                });
            }
        }
    });
}

function notify(msg, icon, type='success', element='body') {
    $.notify({
        icon: icon,
        message: msg,
    }, {
        placement: {
            from: "top",
            align: "center"
        },
        type: type,
        element: element
    });
}
