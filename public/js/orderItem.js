/***********************************************************************************
 *
 *  OrderPage Items & trad Order Page Item
 *
 ************************************************************************************/
 let orderPageType = "";
 let orderPageId = "";
 let itemRow = {};
 let $modalItemIds = {}; // cannot change name coz used in window.operateEvents in bootstrap-table
 $modalItemIds['id'] = new Array('index', 'id', 'progress', 'order_number', 'note', 'customer_due_date', 'factory_due_date', 'received_date');
 $modalItemIds['disabled'] = new Array('progress');
 $modalItemIds['multiple'] = {};
 $modalItemIds['name'] = 'orderItem';
 $modalItemIds['newName'] = '新項目';
 $modalItemIds['main'] = 'orderItem';

var itemButtonOperation = {
  'click .editItem': function (el, id, row, index) {
    editItem(row); // Used in "/project
  },
  'click .svgItem': function (el, id, row, index) {
      viewItemSvg(row); // Used in "/project"
  }
}

function itemButtonColumn() {
  return '<a class="editItem" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;' +
         '<a class="svgItem" href="javascript:void(0)" title="看圖"><i class="fa fa-eye"></i></a>';
}

function viewItemSvg(row) {
  console.log('viewsvg');
  console.log(row);
}

function nyWindowFormatter(value, row) {
  if (value.slice(-1) == "5" || value.slice(-1) == "6") {
    return "<span class='text-info'>" + value + "</span>";
  } else if (value.charAt(1) == "A" || value.charAt(1) == "P") {
    return "<span class='text-info'>" + value + "</span>";
  } else if (!(value.slice(-1) == "3" || value.slice(-1) == "4")) {
    return "<span class='text-danger'>" + value + "</span>";
  }
  return value;
}

function clearOrderItems() {
  $('#order-item-details').html("訂單資料");
  $('#orderItem').bootstrapTable('destroy');
  $('#orderItemTranslation').bootstrapTable('destroy');
  $('#orderItemHistory').bootstrapTable('destroy');
  $('#orderItemShippedHistory').bootstrapTable('destroy');
  $('#toolbar-orderItem button').prop('disabled', 'true');
  clearOrderPageNote();
}

$('#new-orderItem').on('click', function () {
    let row = {'id':0, 'type':"南亞", 'amount':'0','row_number': $('#orderItem').bootstrapTable('getData').length+1};
    editItem(row);
});

// show modal and edit 南亞 order row
function editItem(row) {
    let ignoreList = ['created_at', 'user_id', 'state','type', 'order_page_id'];

    document.getElementById('form-orderItem').reset();
    for (let key of Object.keys(row)) {
        if (key == "id") {
            $('#orderItem-order_item_id').val(row.id);
        } else if (!ignoreList.includes(key)) {
            $('#orderItem-' + key).val(row[key]);
        }
    }
    $('#orderItem-shipped').attr('min', $('#orderItem-shipped').val())
                           .attr('max', $('#orderItem-amount').val());
    $('#orderItem-rank').html(row.row_number);
    $('#orderItem-big_category').trigger('change');
    $('#modal-orderItem').modal('show');

}

function viewOrderItems(row) {
  $('#order-item-details').text('訂單資料: ' + row.order_number);

  if (row.type == "南亞") {
    createBTable('orderItem', 'orderItem', itemRow, true, {id:row.id});
    $('#orderItem-order_page_id').val(row.id);
    $('#toolbar-orderItem button').removeAttr('disabled');

    createOrderItemTranslation(row.id);
    createOrderItemHistory(row.id);
    createOrderItemShippedHistory(row.id);
    displayOrderPageNote(row.id);
    clearOrderPageNote();
  } else if (row.type == "鐵門") {

  } else if (row.type == "門檻") {
      
  }
}

function updateOrderItem() {
    $('#form-orderItem').validate({
        submitHandler: function () {
            let data = {row_number: $('#orderItem-rank').html()};
            $('#form-orderItem input, #form-orderItem select').each(function () {
                data[$(this).attr('name')] = $(this).val();
            });

            $.post('orderItem/update', data, function (result) {
                if (data.progress == "已出貨") {
                    data.shipped = data.amount;
                } else if (data.shipped == data.amount) {
                    data.progress = "已出貨";
                }

                if (data.order_item_id == "0") {
                    data.id = result.id;
                    addBTableRow('orderItem', data);
                } else {
                    updateBTableRow('orderItem', data, data.row_number-1);
                }
                $('#modal-orderItem').modal('hide');
                $('#orderItem tfoot .th-inner span').html();

                createOrderItemTranslation(data.order_page_id);
                createOrderItemHistory(data.order_page_id);
                createOrderItemShippedHistory(data.order_page_id);

                // Update OrderPage Table
                let orderPageData = [];
                orderPageData = $('#orderPage').bootstrapTable('getData');
                for (i=0; i<orderPageData.length; i++) {
                    if (orderPageData[i].id == data.order_page_id) {
                        orderPageData[i].progress = result.progress;
                        orderPageData[i].changeable = result.changeable;

                        updateBTableRow('orderPage', orderPageData[i], i);
                        break;
                    }
                }
            });
            return false;
        }
    });
}

$('#orderItem-big_category').on('change',function () {
    if ($(this).val() == "BN") {
        $('#form-orderItem .frame').removeClass('hide');
        $('#form-orderItem .door').addClass('hide');
    } else if ($(this).val() == "BS") {
        $('#form-orderItem .door').removeClass('hide');
        $('#form-orderItem .frame').addClass('hide');
    } else {
        $('#form-orderItem .door').addClass('hide');
        $('#form-orderItem .frame').addClass('hide');
    }
});

function dateFormatter(value, row) {
    if ("0" == row.leftover) {
        return "<span class='bg-success'>" + moment(value).format('YYYY-MM-DD hh:mm a') + "</span>";
    }
    return moment(value).format('YYYY-MM-DD hh:mm a');
}

function totalShippedFormatter(value, row) {
    if ("0" == row.leftover) {
        return "<span class='text-success'>" + value + "</span>";
    }
    return value;
}

function createOrderItemTranslation(id) {
    $.post('/orderItemTranslation', {'id':id}, function (result) {
        $('#orderItemTranslation').bootstrapTable('destroy').bootstrapTable({
            columns: result.header,
            data: result.row,
            theadClasses: this.theadClasses,
            exportDataType: 'all',
            exportTypes: ['json', 'csv', 'txt'],
            search: true,
            visibleSearch: true,
            showSearchClearButton: true,
            showToggle: true,
            showFullscreen: true,
            showExport: true,
            showPrint: true,
            sortable: true,
            pagination: true,
            pageSize: 20,
            fixedColumns: true,
            fixedNumber: 2,
        });
    });
}

function createOrderItemHistory(id) {
    $.post('/orderItemHistory', {'id':id}, function (result) {
        $('#orderItemHistory').bootstrapTable('destroy').bootstrapTable({
            columns: result.header,
            data: result.row,
            theadClasses: this.theadClasses,
            exportDataType: 'all',
            exportTypes: ['json', 'csv', 'txt'],
            search: true,
            visibleSearch: true,
            showSearchClearButton: true,
            showToggle: true,
            showFullscreen: true,
            showExport: true,
            showPrint: true,
            sortable: true,
            pagination: true,
            pageSize: 50,
        });
    });
}

function createOrderItemShippedHistory(id) {
    $.post('/orderItemHistory/shippedHistory', {'id':id}, function (result) {
        $('#orderItemShippedHistory').bootstrapTable('destroy').bootstrapTable({
            columns: result.header,
            data: result.row,
            theadClasses: this.theadClasses,
            exportDataType: 'all',
            exportTypes: ['json', 'csv', 'txt'],
            search: true,
            visibleSearch: true,
            showSearchClearButton: true,
            showFullscreen: true,
            showExport: true,
            showPrint: true,
            sortable: true,
            pagination: true,
            pageSize: 50,
        });
    });
}
