/********************************************************
 * Order Page related
 ********************************************************/
let $orderPage = $('#orderPage');

 let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
 let $modalIds = {};
 $modalIds['id'] = new Array('index', 'id', 'type', 'progress', 'order_number', 'note', 'customer_due_date', 'factory_due_date', 'received_date');
 $modalIds['disabled'] = new Array('type');
 $modalIds['multiple'] = {};
 $modalIds['name'] = 'orderPage';
 $modalIds['newName'] = '新客戶';
 $modalIds['main'] = 'orderPage';

 var buttonOperation = {
   'click .edit': function (el, id, row, index) {
     showTableModal('orderPage', row, index, $modalIds);
     if (!(row.changeable == 1)) {
        $('#orderPage-type').prop('disabled', 'true');
     } else {
        $('#orderPage-type').removeAttr('disabled');
     }
   },
   'click .view': function (el, id, row, index) {
     let nthRow = index + 1;
     $('#orderPage tbody tr').removeClass('bg-warning');
     $('#orderPage tbody tr:nth-of-type(' + nthRow + ')').addClass('bg-warning');
     viewOrderItems(row); // From orderItem.js
   }
 }

 function buttonColumn() {
   return  '<a class="view" href="javascript:void(0)" title="View"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;' +
           '<a class="edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>';
 }

 const orderPageShowUrl = 'orderPage/show';

jQuery(function(){ Codebase.helpers('datepicker'); });
createBTable('orderPage', orderPageShowUrl, sRow, true, {id:0});

$('#project').on('change', function () {
  var id = $(this).val();
  var url = orderPageShowUrl;

  if (0 != id && "" != id) {
    createBTable('orderPage', url, sRow, true, {id:id});
    $('#orderPage-id').val(id);
  }
  clearOrderItems();

  if (id != "") {
     $('#new-orderPage').removeAttr('disabled');
  } else {
     $('#new-orderPage').prop('disabled', 'true');
  }
});

 // this function is used by bootstrap table
 function customerDateFormatter (value, row) {
   var today = new Date();
   var customerDate = new Date(value);
   var noOfDays = Math.round((customerDate.getTime() - today.getTime()) / (1000 * 3600 * 24));
   const progresses = ["已出貨", "等出貨", "取消訂單", "等SKIN板"];

   if (progresses.includes(row.progress)) {
     return value;
   } else if (value == "0000-00-00") {
     return '<div class="p1 text-danger">' + value + '</div>';
   } else if (today >= customerDate) {
     return '<div class="p1 text-dark bg-danger">' + value + '</div>';
   } else if (noOfDays <= 7) {
     return '<div class="p1 bg-warning" title="' + value + '">剩' + noOfDays + '天</div>';
   } else if (noOfDays <= 14) {
     return '<div class="p1" title="' + value + '">剩' + noOfDays + '天</div>';
   }

   return value;
 }

 // this function is used by bootstrap table
 function progressFormatter (value, row) {
   var klass = "";

   if (row.progress == "收到訂單") {
     klass = "primary";
   } else if (row.progress == "發包中") {
     klass = "info";
   } else if (row.progress == "等排程") {
     klass = "brown";
   } else if (row.progress == "生產中") {
     klass = "violet";
   } else if (row.progress == "烤漆包裝") {
     klass = "purple";
   } else if (row.progress == "等出貨") {
     klass = "light-green";
   } else if (row.progress == "已出貨") {
     klass = "success";
   } else if (row.progress == "等客戶回復") {
     klass = "danger";
   } else if (row.progress == "取消訂單") {
     klass = "danger";
   } else if (row.progress == "等SKIN板") {
     klass = "danger";
   } else {
     klass = "secondary";
   }

   if (row.progress == value) {
     klass = "p-1 text-white bg-" + klass;
   } else {
     klass = 'text-' + klass;
   }

   return '<div class="' + klass + '">' + value + '</div>';
 }

 // Used in OrderPage Modal
 function editOrderPage() {
   $('#form-orderPage').validate({
     submitHandler: function () {
         var url  = "orderPage/edit";
         var data = {
                 project_id: $('#project').val(),
                 id: $('#orderPage-id').val(),
                 standard: $('#orderPage-standard').val(),
                 note: $('#orderPage-note').val(),
                 type: $('#orderPage-type').val(),
                 order_number: $('#orderPage-order_number').val(),
                 orderPage: $('#orderPage-note').val(),
                 customer_due_date: $('#orderPage-customer_due_date').val(),
                 received_date: $('#orderPage-received_date').val()
         }

         $.post(url, data, function (result) {
             var message = "";
             var icon = "";

             if (data.id == "0" || data.id == "") {
                 data.id = result.id;
                 data.progress = "收到訂單";
                 data.frame = 0;
                 data.door = 0;
                 data.threshold = 0;

                 addBTableRow('orderPage', data);
             } else {
                 updateBTableRow('orderPage', data, $('#orderPage-index').val());
             }
             $('#modal-orderPage').modal('hide');
         });
     }
   });
 }


