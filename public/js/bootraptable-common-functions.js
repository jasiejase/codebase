function notify(msg, icon, type='success', element='body') {
    $.notify({
        icon: icon,
        message: msg,
    }, {
        placement: {
            from: "top",
            align: "center"
        },
        type: type,
        element: element
    });
}

function setTotalText() {
    return "總數:";
}

function getColumnCount(data) {
    return data.length;
}

function getColumnSum(data) {
    var field = this.field;
    return '<span class="text-dark">' + data.map(function (row) { return + row[field] })
               .reduce(function (sum, i) { return sum + i }, 0) + '</span>';
}
