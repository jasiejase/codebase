class HOTable {
    constructor (tableId, headers, columns, rows) {
        this.tableId = tableId;
        this.headers = headers;
        this.columns = columns;
        this.rows    = rows;
        this.hiddenColumns = [0];
        this.fixedColumns = 0;
        this.saveURL = '';
        this.orderPageId = '';
        this.editable = false;
        this.exportable = false;
        this.hot = '';
    }

    setHiddenColumns(keys) {
        this.hiddenColumns = keys;
    }

    setFixedColumns(digit) {
        this.fixedColumns = digit;
    }

    setEditable(editable, url, orderPageId) {
        this.editable = editable;
        this.saveURL = url;
        this.orderPageId = orderPageId;
    }

    showExport(show) {
        this.exportable = show;
    }

    getInstance() {
        return this.hot;
    }

    createHOTableButton(buttonId, btnType, buttonText) {
        var id = this.tableId.id
        $('#toolbar-' + id).append('<button id="'+buttonId+'-'+id+'" class="btn btn-'+btnType+' mr-5 mb-5">'+buttonText+'</button>');
    }

    create() {
            this.hot = new Handsontable(this.tableId, {
            language: 'zh-TW',
            data: this.rows,
            colHeaders: this.headers,
            columns: this.columns,
            rowHeaders: false,
            hiddenColumns: {
                columns:this.hiddenColumns,
                copyPasteEnabled: false
            },
            fixedColumnsLeft: this.fixedColumns,
            columnSorting: true,
            filters: true,
            dropdownMenu: ['filter_by_condition', 'filter_action_bar', 'filter_by_value'],
            width:'100%',
            height: 'auto',
            licenseKey: 'non-commercial-and-evaluation',
            beforeChange(changes) { //change colour if cell has been changed
                changes.forEach((change) => {
                const [row, column, oldValue, newValue] = change;

                if (oldValue != newValue)
                    this.setCellMeta(row, this.propToCol(column), 'className', 'colored');
                });
            },  
        });

        if (this.editable) {
            this.createHOTableButton('save','success','<i class="fa fa-save mr-5"></i>Save訂單');
            this.createHOTableButton('new','primary','<i class="fa fa-plus mr-5"></i>新增項目');
            this.createHOTableButton('undo','secondary','<i class="fa fa-rotate-left"></i>');
            this.createHOTableButton('redo','secondary','<i class="fa fa-rotate-right"></i>');
            
            const save = document.querySelector('#save-'+this.tableId.id);
            const add  = document.querySelector('#new-'+this.tableId.id);

            // Save orders
            Handsontable.dom.addEvent(save, 'click', () => {
                if ($('td').hasClass('htInvalid')) {
                    console.log("請修改錯誤(紅色部分)");
                } else {
                    var data = {id: this.orderPageId, rows: JSON.stringify(this.hot.getData()) };
                    $.post(this.saveURL, data, function(res) {
                        $('td').removeClass('colored');
                        console.log(res);
                    });
                }
            });

            // Add new Rows
            Handsontable.dom.addEvent(add, 'click', () => {
                var row = this.hot.countRows();
                this.hot.alter('insert_row');
                this.hot.setDataAtCell(row, 0, 0);
                this.hot.setDataAtCell(row, 1, ++row);
            });

            // Undo Redo
            $('#undo-' + this.tableId.id).on('click', () => { this.hot.undo(); });
            $('#redo-' + this.tableId.id).on('click', () => { this.hot.redo(); });
        }

        // Add CSV export
        if (this.exportable) {
            this.createHOTableButton('csv','secondary','<i class="fa fa-file mr-5"></i>下載資料');
            const exportPlugin = this.hot.getPlugin('exportFile');
            $('#csv-' + this.tableId.id).on('click', () => {
                exportPlugin.downloadFile('csv', {
                    bom: true,
                    columnDelimiter: ',',
                    columnHeaders: true,
                    exportHiddenColumns: false,
                    exportHiddenRows: false,
                    fileExtension: 'csv',
                    filename: 'CSV-file_[YYYY]-[MM]-[DD]',
                    mimeType: 'text/csv',
                    rowDelimiter: '\r\n',
                    rowHeaders: false
                });
            });
        }
    }
}

