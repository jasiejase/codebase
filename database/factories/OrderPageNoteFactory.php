<?php

namespace Database\Factories;

use App\Models\OrderPageNote;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderPageNoteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderPageNote::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
