<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('category');
            $table->string('unit');
            $table->integer('undersupply');
            $table->string('size');
            $table->text('note');
            $table->timestamps();
        });

        Schema::create('material_suppliers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('supplier_id')->constrained('suppliers');
            $table->foreignId('material_id')->constrained('materials');
            $table->boolean('available')->default(true);
            $table->timestamps();
        });

        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('material_supplier_id')->constrained('material_suppliers');
            $table->string('name');
            $table->float('price')->default('-1.00');
            $table->date('date')->nullable()->default(null);
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
        Schema::dropIfExists('material_suppliers');
        Schema::dropIfExists('materials');
    }
}
