<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('company');
            $table->string('gui');
            $table->text('address');
            $table->string('phone');
            $table->string('phone2')->default('');
            $table->string('fax');
            $table->string('website');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('company_corporation', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained('companies');
            $table->foreignId('corporation_id')->constrained('corporations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_corporation');
        Schema::dropIfExists('companies');
    }
}
