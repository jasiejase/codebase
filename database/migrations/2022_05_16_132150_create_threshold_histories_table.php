<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThresholdHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threshold_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('threshold_id')->constrained('threholds');
            $table->foreignId('user_id')->constrained('users');
            $table->string('field');
            $table->string('old_value');
            $table->string('new_value');
            $table->timestamps();
        });

        DB::unprepared("
            CREATE TRIGGER `thresholds_BEFORE_UPDATE` BEFORE UPDATE ON `thresholds` FOR EACH ROW BEGIN
                IF old.name <> new.name THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'name', old.name, new.name, NOW(), NOW());
                END IF;
                IF old.amount <> new.amount THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'amount', old.amount, new.amount, NOW(), NOW());
                END IF;
                IF old.shipped <> new.shipped THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'shipped', old.shipped, new.shipped, NOW(), NOW());
                END IF;
                IF old.material <> new.material THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'material', old.material, new.material, NOW(), NOW());
                END IF;
                IF old.length <> new.length THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'length', old.length, new.length, NOW(), NOW());
                END IF;
                IF old.shape <> new.shape THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'shape', old.shape, new.shape, NOW(), NOW());
                END IF;
                IF old.a <> new.a THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'a', old.a, new.a, NOW(), NOW());
                END IF;
                IF old.b <> new.b THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'b', old.b, new.b, NOW(), NOW());
                END IF;
                IF old.c <> new.c THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'c', old.c, new.c, NOW(), NOW());
                END IF;
                IF old.d <> new.d THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'd', old.d, new.d, NOW(), NOW());
                END IF;
                IF old.e <> new.e THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'e', old.e, new.e, NOW(), NOW());
                END IF;
                IF old.e <> new.e THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'e', old.e, new.e, NOW(), NOW());
                END IF;
                IF old.f <> new.f THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'f', old.f, new.f, NOW(), NOW());
                END IF;
                IF old.airtight_strip <> new.airtight_strip THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'airtight_strip', old.airtight_strip, new.airtight_strip, NOW(), NOW());
                END IF;
                IF old.price <> new.price THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'price', old.price, new.price, NOW(), NOW());
                END IF;
                IF old.progress <> new.progress THEN
                    INSERT INTO threhold_histories (threshold_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'progress', old.progress, new.progress, NOW(), NOW());
                END IF;
            END
        ");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `thresholds_BEFORE_UPDATE`');
        Schema::dropIfExists('threshold_histories');
    }
}
