<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThresholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thresholds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_page_id')->constrained('order_pages');
            $table->foreignId('user_id')->constrained('users');
            $table->integer('row_number');
            $table->string('name');
            $table->integer('amount');
            $table->integer('shipped');
            $table->string('material');
            $table->float('length')->default(0);
            $table->string('shape');
            $table->float('a')->default(0);
            $table->float('b')->default(0);
            $table->float('c')->default(0);
            $table->float('d')->default(0);
            $table->float('e')->default(0);
            $table->float('f')->default(0);
            $table->string('airtight_strip')->default("");
            $table->integer('price');
            $table->string('progress')->default('收到訂單');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thresholds');
    }
}
