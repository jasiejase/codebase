<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trad_order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_page_id')->constrained('order_pages');
            $table->string('row_number');
            $table->string('name');
            $table->enum('door_frame', ['框', '扇', '門檻']);
            $table->string('type');
            $table->string('colour');
            $table->float('width');
            $table->float('height');
            $table->integer('amount_l');
            $table->integer('amount_r');
            $table->string('lock');
            $table->string('handle');
            $table->integer('handle_height')->default(1000);
            $table->string('hinge');
            $table->string('closer');
            $table->string('bolt');
            $table->boolean('fail_safe_lock');
            $table->string('fail_safe_lock_distance')->default(100);
            $table->boolean('fail_safe_lock_both')->default(false);
            $table->boolean('magnetic_hole');
            $table->string('magnetic_hole_distance')->default(100);
            $table->boolean('magnetic_hole_both')->default(false);
            $table->string('falling_strip');
            $table->string('ball');
            $table->string('material');
            $table->integer('door_thickness')->default(49);
            $table->text('note');
            $table->date('due_date');
            $table->string('progress');
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });

        Schema::create('trad_order_frame_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('trad_order_item_id')->constrained('trad_order_items');
            $table->enum('coupled', ['單開', '雙開']);
            $table->float('mother_width');
            $table->float('depth');
            $table->string('shape');
            $table->float('f_a')->comment('v_total');
            $table->float('f_b')->comment('v_north');
            $table->float('f_c')->comment('v_west');
            $table->float('f_d')->comment('v_east');
            $table->float('f_e')->comment('v_south_east');
            $table->float('f_t')->comment('v_door_thickness for frame');
            $table->float('h_a')->comment('h_total');
            $table->float('h_b')->comment('h_north');
            $table->float('h_c')->comment('h_west');
            $table->float('h_d')->comment('h_east');
            $table->float('h_e')->comment('h_south_east');
            $table->float('h_t')->comment('h_door_thickness for frame');
            $table->string('reinforce_iron')->default('');
            $table->string('smoke_strip')->default('');
            $table->string('airtight_strip')->default('');
            $table->boolean('fill_hole')->default(0)->comment('四面框補破口');
            $table->timestamps();
        });

        Schema::create('trad_order_door_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('trad_order_item_id')->constrained('trad_order_items');
            $table->integer('related_frame')->comment('連接門框 tradOrderItems ID');
            $table->enum('is_mother', ['單', '母', '子']);
            $table->string('origin')->comment('彈射門補強位置');
            $table->string('width')->comment('彈射門補強寬');
            $table->string('height')->comment('彈射門補強高');
            $table->integer('horizontal')->comment('彈射門補強位置');
            $table->integer('vertical')->comment('彈射門補強位置');
            $table->integer('glass_x');
            $table->integer('glass_y');
            $table->integer('glass_width');
            $table->integer('glass_height');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trad_order_door_items');
        Schema::dropIfExists('trad_order_frame_items');
        Schema::dropIfExists('trad_order_items');
    }
}
