<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('supplier_id')->constrained('suppliers');
            $table->string('address');
            $table->text('note');
            $table->date('order_date');
            $table->foreignId('user_id')->constrained('users');
            $table->boolean('approved_by_1')->default(false);
            $table->boolean('approved_by_2')->default(false);
            $table->timestamps();
        });

        Schema::create('material_shipment', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shipment_id')->constrained('shipments');
            $table->foreignId('material_id')->constrained('materials');
            $table->string('price');
            $table->float('amount');
            $table->date('received_at')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_shipment');
        Schema::dropIfExists('shipments');
    }
}
