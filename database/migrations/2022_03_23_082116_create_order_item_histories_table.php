<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_item_id')->constrained('order_items');
            $table->foreignId('user_id')->constrained('users');
            $table->string('field');
            $table->string('old_value');
            $table->string('new_value');
            $table->timestamps();
        });

        DB::unprepared("
            CREATE TRIGGER `order_items_BEFORE_UPDATE` BEFORE UPDATE ON `order_items` FOR EACH ROW BEGIN
                IF old.name <> new.name THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'name', old.name, new.name, NOW(), NOW());
                END IF;
                IF old.big_category <> new.big_category THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'big_category', old.big_category, new.big_category, NOW(), NOW());
                END IF;
                IF old.middle_category <> new.middle_category THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'middle_category', old.middle_category, new.middle_category, NOW(), NOW());
                END IF;
                IF old.window <> new.window THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'window', old.window, new.window, NOW(), NOW());
                END IF;
                IF old.width <> new.width THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'width', old.width, new.width, NOW(), NOW());
                END IF;
                IF old.up_window <> new.up_window THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'up_window', old.up_window, new.up_window, NOW(), NOW());
                END IF;
                IF old.total_height <> new.total_height THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'total_height', old.total_height, new.total_height, NOW(), NOW());
                END IF;
                IF old.wind <> new.wind THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'wind', old.wind, new.wind, NOW(), NOW());
                END IF;
                IF old.colour <> new.colour THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'colour', old.colour, new.colour, NOW(), NOW());
                END IF;
                IF old.amount <> new.amount THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'amount', old.amount, new.amount, NOW(), NOW());
                END IF;
                IF old.price <> new.price THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'price', old.price, new.price, NOW(), NOW());
                END IF;
                IF old.threshold <> new.threshold THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'threshold', old.threshold, new.threshold, NOW(), NOW());
                END IF;
                IF old.mixed_frame <> new.mixed_frame THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'mixed_frame', old.mixed_frame, new.mixed_frame, NOW(), NOW());
                END IF;
                IF old.Box <> new.Box THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'Box', old.Box, new.Box, NOW(), NOW());
                END IF;
                IF old.L1 <> new.L1 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'L1', old.L1, new.L1, NOW(), NOW());
                END IF;
                IF old.L2 <> new.L2 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'L2', old.L2, new.L2, NOW(), NOW());
                END IF;
                IF old.L3 <> new.L3 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'L3', old.L3, new.L3, NOW(), NOW());
                END IF;
                IF old.L4 <> new.L4 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'L4', old.L4, new.L4, NOW(), NOW());
                END IF;
                IF old.L5 <> new.L5 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'L5', old.L5, new.L5, NOW(), NOW());
                END IF;
                IF old.depth <> new.depth THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'depth', old.depth, new.depth, NOW(), NOW());
                END IF;
                IF old.lock <> new.lock THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'lock', old.lock, new.lock, NOW(), NOW());
                END IF;
                IF old.lock_height <> new.lock_height THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'lock_height', old.lock_height, new.lock_height, NOW(), NOW());
                END IF;
                IF old.f1 <> new.f1 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'f1', old.f1, new.f1, NOW(), NOW());
                END IF;
                IF old.f2 <> new.f2 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'f2', old.f2, new.f2, NOW(), NOW());
                END IF;
                IF old.f3 <> new.f3 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'f3', old.f3, new.f3, NOW(), NOW());
                END IF;
                IF old.f4 <> new.f4 THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'f4', old.f4, new.f4, NOW(), NOW());
                END IF;
                IF old.due_date <> new.due_date THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'due_date', old.due_date, new.due_date, NOW(), NOW());
                END IF;
                IF old.progress <> new.progress THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'progress', old.progress, new.progress, NOW(), NOW());
                END IF;
                IF old.shipped <> new.shipped THEN
                    INSERT INTO order_item_histories (order_item_id, user_id, field, old_value, new_value, created_at, updated_at)
                    VALUES (old.id, new.user_id, 'shipped', old.shipped, new.shipped, NOW(), NOW());
                END IF;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `order_items_BEFORE_UPDATE`');
        Schema::dropIfExists('order_item_histories');
    }
}
