<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('project_id')->constrained('projects');
            $table->string('order_number');
            $table->string('standard')->default('CNS 11227-1');
            $table->string('note');
            $table->date('customer_due_date');
            $table->date('factory_due_date');
            $table->date('received_date');
            $table->string('progress');
            $table->enum('type',['NY', 'YS', 'TH']);
            $table->boolean('changeable')->default(true)->comment('set false if cannot change order type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pages');
    }
}
