<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_page_id')->constrained('order_pages');
            $table->string('row_number');
            $table->string('name');
            $table->string('big_category');
            $table->string('middle_category');
            $table->string('window');
            $table->integer('width');
            $table->integer('up_window');
            $table->integer('total_height');
            $table->string('wind');
            $table->string('colour');
            $table->integer('amount');
            $table->integer('shipped')->default(0);
            $table->integer('price');
            $table->string('threshold');
            $table->string('mixed_frame');
            $table->string('box');
            $table->integer('L1');
            $table->integer('L2');
            $table->integer('L3');
            $table->integer('L4');
            $table->integer('L5');
            $table->integer('depth');
            $table->string('lock');
            $table->integer('lock_height');
            $table->string('f1');
            $table->string('f2');
            $table->string('f3');
            $table->string('f4');
            $table->string('reinforce_iron')->default('');
            $table->string('smoke_strip')->default('');
            $table->string('airtight_strip')->default('');
            $table->foreignId('user_id')->constrained('users');
            $table->date('due_date');
            $table->string('progress');
            $table->timestamps();
        });

        Schema::create('ny_middle_categories', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_up_windows', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_door_types', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_directions', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });


        Schema::create('ny_frame_types', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
            $table->text('svg');
            $table->text('comment');
        });

        Schema::create('ny_colours', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
            $table->string('name');
            $table->string('style');
        });

        Schema::create('ny_hinges', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_bolts', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_up_boxes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        Schema::create('ny_down_boxes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });

        // 五金表
        Schema::create('ny_hardware', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('translation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('ny_middle_categories');
        Schema::dropIfExists('ny_up_windows');
        Schema::dropIfExists('ny_door_types');
        Schema::dropIfExists('ny_directions');
        Schema::dropIfExists('ny_frame_types');
        Schema::dropIfExists('ny_colours');
        Schema::dropIfExists('ny_hinges');
        Schema::dropIfExists('ny_bolts');
        Schema::dropIfExists('ny_up_boxes');
        Schema::dropIfExists('ny_down_boxes');
        Schema::dropIfExists('ny_hardware');
    }
}
