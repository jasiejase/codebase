const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    /* CSS */
    .sass('resources/sass/main.scss', 'public/css/codebase.css')
    .sass('resources/sass/codebase/themes/corporate.scss', 'public/css/themes/')
    .sass('resources/sass/codebase/themes/earth.scss', 'public/css/themes/')
    .sass('resources/sass/codebase/themes/elegance.scss', 'public/css/themes/')
    .sass('resources/sass/codebase/themes/flat.scss', 'public/css/themes/')
    .sass('resources/sass/codebase/themes/pulse.scss', 'public/css/themes/')

    /* JS */
    .js('resources/js/app.js', 'public/js/laravel.app.js')
    .js('resources/js/codebase/app.js', 'public/js/codebase.app.js')
    .js('resources/js/codebase/modules/helpers.js', 'public/js/codebase.helpers.js')

    /* Page JS */
    .js('resources/js/pages/tables_datatables.js', 'public/js/pages/tables_datatables.js')

    /* Tools */
    .browserSync('localhost:8000')
    .disableNotifications()

    /* Bootstrap-table */
    .scripts(['node_modules/bootstrap-table/dist/bootstrap-table.min.js',
         'node_modules/bootstrap-table/dist/bootstrap-table-locale-all.min.js',
         'node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js',
         'node_modules/bootstrap-table/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.js',
         'node_modules/bootstrap-table/dist/extensions/print/bootstrap-table-print.min.js',
         'resources/js/tableExport.min.js'
        ], 'public/js/bootstrap-table.combine.js')
    .styles(['node_modules/bootstrap-table/dist/bootstrap-table.css',
             'node_modules/bootstrap-table/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.css'
            ], 'public/css/bootstrap-table.combine.css')
    
    /* Options */
    .options({
        processCssUrls: false
    });
