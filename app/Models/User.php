<?php

namespace App\Models;

use App\Casts\EncryptCast;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'corporation_id',
        'name',
        'mobile',
        'email',
        'position',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'id' => EncryptCast::class,
        'email_verified_at' => 'datetime'
    ];

    /**
     * Set up Accessors
     */
    public function getIsActiveAttribute($value)
    {
        return ($value) ? "活躍" : "停用";
    }

    /**
     * Set up Mutators
     */
    public function setIsActiveAttribute($value)
    {
        $this->attributes['is_active'] = ($value == "活躍") ? 1 : 0;
    }

    /**
     * Set up table relationship
     */
    public function corporations()
    {
        return $this->BelongsToMany('App\Models\Corporation')->withTimestamps();
    }
}

