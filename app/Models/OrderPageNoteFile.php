<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPageNoteFile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Set up table relationship
     */
    public function orderPageNote()
    {
        return $this->BelongsTo('App\Models\OrderPageNote');
    }
}
