<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Threshold extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Set up table relationship
     */
    public function orderItem()
    {
        return $this->BelongsTo('App\Models\OrderItem');
    }

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}
