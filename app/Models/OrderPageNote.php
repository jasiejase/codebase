<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPageNote extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Set up table relationship
     */
    public function orderPage()
    {
        return $this->BelongsTo('App\Models\OrderPage');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function orderPageNoteFiles()
    {
        return $this->hasMany('App\Models\OrderPageNoteFile');
    }
}
