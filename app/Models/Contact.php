<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Set up Relationships
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project')->withTimestamps();
    }
}
