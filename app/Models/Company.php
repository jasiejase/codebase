<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company',
        'gui',
    ];

    /**
     * Set up table relationships
     */
    public function corporations()
    {
        return $this->belongsToMany('App\Models\Corporation')->withTimestamps();
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}
