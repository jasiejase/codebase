<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TradOrderItem extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'date_created',
        'updated_at'
    ];

    /**
     * Set up table relationship
     */
    public function orderPage()
    {
        return $this->BelongsTo('App\Models\OrderPage');
    }

    public function tradFrameOrderItem()
    {
        return $this->hasOne('App\Models\TradFrameOrderItem');
    }
    
    public function tradDoorOrderItem()
    {
        return $this->hasOne('App\Models\TradDoorOrderItem');
    }
    
    public function tradThresholdOrderItem()
    {
        return $this->hasOne('App\Models\TradThresholdOrderItem');
    }
}
