<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThresholdHistory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Set up table relationship
     */
    public function threshold()
    {
        return $this->BelongsTo('App\Models\Threshold');
    }

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}
