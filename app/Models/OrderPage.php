<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OrderPage extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'order_number',
        'received_date',
        'progress',
        'progress_date',
        'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'date_created',
        'updated_at'
    ];

    /**
     * Set up Accessors
     */
    public function getTypeAttribute($value)
    {
        switch ($value)
        {
            case "NY":
                return "南亞";
            case "YS":
                return "鐵門";
            case "TH":
                return "門檻";
        }
    }

    /**
     * Set up Mutators
     */
    public function setTypeAttribute($value)
    {
        switch ($value)
        {
            case "南亞":
                $this->attributes['type'] = "NY";
                break;
            case "鐵門":
                $this->attributes['type'] = "YS";
                break;
            case "門檻":
                $this->attributes['type'] = "TH";
                break;
        }
    }

    /**
     * Set up table relationship
     */
    public function project()
    {
        return $this->BelongsTo('App\Models\Project');
    }

    public function orderPageNotes()
    {
        return $this->hasMany('App\Models\OrderPageNote');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function tradOrderItems()
    {
        return $this->hasMany('App\Models\TradOrderItem');
    }

    public function thresholds()
    {
        return $this->hasMany('App\Models\Threshold');
    }
}
