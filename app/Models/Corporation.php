<?php

namespace App\Models;

use App\Casts\EncryptCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'id' => EncryptCast::class
    ];

    /**
     * Set up table relationship
     */

     public function users()
     {
         return $this->belongsToMany('App\Models\User');
     }

     public function companies()
     {
         return $this->belongsToMany('App\Models\Company');
     }
}
