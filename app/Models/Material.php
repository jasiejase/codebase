<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Material extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Set up table relationship
     */
    public function suppliers()
    {
        return $this->hasMany('App\Models\MaterialSupplier')->with('Supplier');
    }

    public function shipments()
    {
        return $this->belongsToMany('App\Models\Shipments')->withPivot('price', 'amount', 'received_at');
    }
}
