<?php

namespace App\Http\Controllers;

use App\Models\Threshold;
use Illuminate\Http\Request;
use Response;

class ThresholdHistoryController extends Controller
{
    // view 門檻 order Item History
    public function index(Request $request)
    {
        $id = $request->id; // Order Page ID

        $headers = array();
        $headers[] = array('field'=> 'created_at', 'title'=> '修改日', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'dateFormatter');
        $headers[] = array('field'=> 'row_number', 'title'=> "項次", 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'model', 'title'=> '品項', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'name', 'title'=> '修改者', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'name_tw', 'title'=> '更改項目', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'old_value', 'title'=> '更改前', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'new_value', 'title'=> '更改後', 'sortable'=> true, 'align'=> 'center');

        $rows = Threshold::select('row_number', 'thresholds.name AS model', 'users.name', 'name_tw', 'old_value', 'new_value', 'threshold_histories.created_at')
                        ->join('threshold_histories', 'threshold_histories.threshold_id', '=', 'thresholds.id')
                        ->join('users', 'users.id', '=', 'threshold_histories.user_id')
                        ->join('field_translations', 'field_translations.field', '=', 'threshold_histories.field')
                        ->where('order_page_id', $id)
                        ->where('threshold_histories.field','!=', 'shipped')
                        ->orderBy('threshold_histories.created_at')->get();

        return Response::json(array(
            'header' => $headers,
            'row' => $rows
        ));
    }

    public function shippedHistory(Request $request)
    {
        $id = $request->id; // Order Page ID

        $headers = array();
        $headers[] = array('field'=> 'date', 'title'=> '修改日', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'dateFormatter');
        $headers[] = array('field'=> 'row_number', 'title'=> "項次", 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');
        $headers[] = array('field'=> 'model', 'title'=> '品項', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');
        $headers[] = array('field'=> 'shipped_total', 'title'=> '已出貨', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');
        $headers[] = array('field'=> 'shipped', 'title'=> '含這次出貨', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');
        $headers[] = array('field'=> 'leftover', 'title'=> '未出貨', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');
        $headers[] = array('field'=> 'name', 'title'=> '出貨者', 'sortable'=> true, 'align'=> 'center', 'formatter'=>'totalShippedFormatter');

        $shipped = OrderItem::select('row_number', 'thresholds.name AS model', 'users.name', 'amount', 'old_value', 'new_value', 'threshold_histories.created_at')
                        ->join('threshold_histories', 'threshold_histories.threshold_id', '=', 'thresholds.id')
                        ->join('users', 'users.id', '=', 'threshold_histories.user_id')
                        ->where('order_page_id', $id)
                        ->where('threshold_histories.field','=', 'shipped')
                        ->orderBy('threshold_histories.created_at')->get();

        $rows = array();
        foreach ($shipped as $row)
        {
            $data = array();
            $data['row_number'] = $row->row_number;
            $data['model'] = $row->model;
            $data['window'] = $row->window;
            $data['name'] = $row->name;
            $data['shipped'] = $row->new_value - $row->old_value;
            $data['shipped_total'] = $row->new_value;
            $data['leftover'] = $row->amount - $row->new_value;
            $data['date'] = date("Y-m-d H:i", strtotime($row->created_at));
            $rows[] = $data;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $rows
        ));
    }
}
