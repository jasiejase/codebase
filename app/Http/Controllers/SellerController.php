<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;
use Response;

class SellerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Contact list in Modal
     */
    public function index(Request $request)
    {
        $supplier_id = $request->id;
        $sellers = Seller::where('supplier_id', $supplier_id)->select('id', 'name')->get();

        return Response::json(array(
            'sellers' => $sellers
        ));
    }

    /**
     * Show per Contact in Modal
     */
    public function show(Request $request)
    {
        $seller = Seller::find($request->id);
        return Response::json(array(
            'seller' => $seller
        ));
    }

    /**
     * Create & Update seller
     */
    public function edit(Request $request)
    {
        if ($request->id != 0)
        {
            // update
            $seller = Seller::find($request->id);
        }
        else
        {
            // create
            $seller = new seller;
        }

        $seller->supplier_id = $request->supplier_id;
        $seller->name = $request->name;
        $seller->position = $request->position ?? '';
        $seller->phone = $request->phone ?? '';
        $seller->mobile = $request->mobile ?? '';
        $seller->email = $request->email ?? '';
        $seller->note = $request->note ?? '';
        $seller->save();

        return Response::json(array(
            'id' => $seller->id,
            'changed' => $seller->wasChanged()
        ));
    }
}
