<?php

namespace App\Http\Controllers;

use App\Models\OrderPage;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class OrderPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function ajaxIndex()
    // {
    //     return view('orderPage.ajaxIndex');
    // }

    public function show(Request $request)
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center');
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center');
        $headers[] = array('title'=>'進度', 'field'=>'progress', 'align'=>'center', 'sortable'=>true, 'formatter'=>'progressFormatter');
        $headers[] = array('title'=>'訂單編號', 'field'=>'order_number', 'align'=>'center', 'sortable'=>true, 'formatter'=>'progressFormatter');
        $headers[] = array('title'=>'備註', 'field'=>'note', 'align'=>'left', 'sortable'=>true, 'footerFormatter'=>'setTotalText');
        $headers[] = array('title'=>'框', 'field'=>'frame', 'align'=>'right', 'sortable'=>true, 'footerFormatter'=>'getColumnSum', 'formatter'=>'progressFormatter');
        $headers[] = array('title'=>'扇', 'field'=>'door', 'align'=>'right', 'sortable'=>true, 'footerFormatter'=>'getColumnSum', 'formatter'=>'progressFormatter');
        $headers[] = array('title'=>'門檻', 'field'=>'threshold', 'align'=>'right', 'sortable'=>true, 'footerFormatter'=>'getColumnSum', 'formatter'=>'progressFormatter');
        $headers[] = array('title'=>'客戶需求日', 'field'=>'customer_due_date', 'align'=>'center', 'sortable'=>true, 'formatter'=>'customerDateFormatter');
        $headers[] = array('title'=>'收到日期', 'field'=>'received_date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'訂單類別', 'field'=>'type', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'min-width'=>90, 'events'=>'buttonOperation', 'formatter'=>'buttonColumn', 'clickToSelect'=>false);

        if ("" == $request->id || 0 == $request->id)
        {
            return Response::json(array(
                'header' => $headers,
                'row' => array()
            ));
        }

        $orders = OrderPage::with('orderItems', 'tradOrderItems', 'thresholds')->where('project_id', $request->id)->orderBy('received_date','desc')->get();

        $orders->each( function ($item, $key)
        {
            if ($item['type'] == "南亞")
            {
                $item['frame'] = $item->orderItems->where('big_category', 'BN')->sum('amount');
                $item['door'] = $item->orderItems->where('big_category', 'BS')->sum('amount');
                $item['threshold'] = 0;
            }
            // else if ($item['type'] == "鐵門")
            // {
            //     // let's focus on 南亞 first
            //     // so commented out below

            //     // $frameObj = $item->tradOrderItems->where('door_frame', '框');
            //     // $doorObj  = $item->tradOrderItems->where('door_frame', '扇');

            //     // $item['frame'] = $frameObj->sum('amount_l') + $frameObj->sum('amount_r');
            //     // $item['door']  = $doorObj->sum('amount_l') + $frameObj->sum('amount_r');
            //     // $item['threshold'] = $item->tradOrderItems->where('door_frame', '門檻')->sum('amount_l');
            // }
            else if ($item['type'] == "門檻")
            {
                $item['frame'] = 0;
                $item['door'] = 0;
                $item['threshold'] = $item->thresholds->sum('amount');
            }
            else
            {
                $item['frame'] = 0;
                $item['door'] = 0;
                $item['threshold'] = 0;
            }

            $item['orderItems'] = "";
            $item['tradOrderItems'] = "";

        });

        return Response::json(array(
            'header' => $headers,
            'row' => $orders
        ));
    }

    public function edit(Request $request)
    {
        if (!OrderPage::where('id', $request->id)->exists()) {
            $orderPage = new OrderPage;
            $orderPage->progress = "收到訂單";
            $orderPage->factory_due_date = "1900-01-01";
        } else {
            $orderPage = OrderPage::find($request->id);
        }

        $orderPage->project_id = $request->project_id;
        $orderPage->order_number = $request->order_number;
        $orderPage->standard = $request->standard;
        $orderPage->note = $request->note ?? '';
        $orderPage->type = $request->type;
        $orderPage->customer_due_date = $request->customer_due_date ?? '';
        $orderPage->received_date = $request->received_date;
        $orderPage->save();

        return Response::json(array(
            'id' => $orderPage->id,
            'success' => '1'
        ));
    }


}
