<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Response;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Contact list in Modal
     */
    public function index(Request $request)
    {
        $company_id = $request->id;
        $contacts = Contact::where('company_id', $company_id)->select('id', 'name')->get();

        return Response::json(array(
            'contacts' => $contacts
        ));
    }

    /**
     * Show per Contact in Modal
     */
    public function show(Request $request)
    {
        $contact = Contact::find($request->id);
        return Response::json(array(
            'contact' => $contact
        ));
    }

    /**
     * Create & Update Contact
     */
    public function edit(Request $request)
    {
        if ($request->id != 0)
        {
            // update
            $contact = Contact::find($request->id);
        }
        else
        {
            // create
            $contact = new Contact;
        }

        $contact->company_id = $request->company_id;
        $contact->name = $request->name;
        $contact->position = $request->position ?? '';
        $contact->phone = $request->phone ?? '';
        $contact->mobile = $request->mobile ?? '';
        $contact->email = $request->email ?? '';
        $contact->note = $request->note ?? '';
        $contact->save();

        return Response::json(array(
            'id' => $contact->id,
            'changed' => $contact->wasChanged()
        ));
    }
}
