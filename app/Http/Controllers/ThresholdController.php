<?php

namespace App\Http\Controllers;

use App\Models\OrderPage;
use App\Models\Threshold;
use App\Models\ThresholdHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class ThresholdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $headers = array();
        $headers[] = array('field'=> 'state', 'checkbox'=> true, 'align'=> 'center', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'id', 'title'=> 'ID', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'row_number', 'title'=> "項次", 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'name', 'title'=> '品項', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'progressFormatter', 'width'=> 100);
        $headers[] = array('field'=> 'progress', 'title'=> '進度', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'progressFormatter', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'amount', 'title'=> '數量', 'sortable'=> true, 'align'=> 'right', 'footerFormatter'=> 'getColumnSum', 'formatter'=> 'progressFormatter');
        $headers[] = array('field'=> 'material', 'title'=> '材質', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'length', 'title'=> '長度', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'shape', 'title'=> '形狀', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'airtight_strip', 'title'=> '氣密條', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'price', 'title'=> '單價', 'sortable'=> true, 'align'=> 'center');

        $rows = Threshold::where('order_page_id', $request->id)->get();

        return Response::json(array(
            'header' => $headers,
            'row' => $rows
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $order_page_id = $order_page_id;

        if (0 == $id) // new row
        {
            $threshold = new Threshold;
            $threshold->order_page_id = $request->order_page_id;
            $threshold->user_id = Auth::user()->id;
        }
        else
        {
            $threshold = Threshold::find($id);
        }

        $threshold->row_number = $request->row_number;
        $threshold->name = $request->name;
        $threshold->amount = $request->amount;
        $threshold->material = $request->material;
        $threshold->length = $request->length;
        $threshold->shape = $request->shape;
        $threshold->a = $request->a;
        $threshold->b = $request->b;
        $threshold->c = $request->c;
        $threshold->d = $request->d;
        $threshold->e = $request->e;
        $threshold->f = $request->f;
        $threshold->airtight_strip = $request->airtight_strip;
        $threshold->price = $request->price;
        $threshold->progress = $request->progress;
        $threshold->save();

        // Update orderPage Progress
        $orderPageProgress = "";
        $progress = Threshold::where('order_page_id', $order_page_id)->pluck('progress')->unique();
        $orderPage = OrderPage::find($order_page_id);

        if ($progress->count() > 1)
        {
            $orderPage->progress = "<abbr title='" . $progress->implode(', ') . "'>" . $progress->count() . "種進度</abbr>";
            $orderPageProgress = "<abbr title='" . $progress->implode(', ') . "'>" . $progress->count() . "種進度</abbr>";
        }
        else if ($progress->count() == 1)
        {
            $orderPage->progress = $progress[0];
            $orderPageProgress = $progress[0];
        }
        else
        {
            $orderPageProgress = "收到進度";
        }

        $orderPage->save();

        // Create new order History
        if (0 == $id)
        {
            $thresholdHistory = new thresholdHistory;
            $thresholdHistory->threshold_id = $threshold->id;
            $thresholdHistory->user_id = Auth::user()->id;
            $thresholdHistory->field = "progress";
            $thresholdHistory->old_value = "";
            $thresholdHistory->new_value = "收到訂單";
            $thresholdHistory->save();
        }
        return array('id'=>$threshold->id, 'text'=>'success', 'progress' => $orderPageProgress);
    }
}
