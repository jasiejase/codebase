<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "用戶資料";
        return view('profile.index', $data);
    }

    public function update(Request $request)
    {
        // $validate = $this->validate($request, [
        //     'name' => ['required', 'string', 'max:50'],
        //     'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
        //     'position' => ['required', 'string', 'max:10']
        // ]);

        $input = $request->post();
        $user = User::find(Auth::user()->id);

        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->mobile = $input['mobile'];
        $user->save();

        return response()->json([ 'success' => true ]);
    }

    public function updatePassword(Request $request)
    {
        $input = $request->post();

        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required'
        ]);

        $user = User::find(Auth::user()->id);

        if( ! Hash::check($input['old_password'], $user->password))
        {
            return response()->json([ 'success' => false, 'message' => '密碼修改失敗, 請在確認' ]);
        }

        $user->password = Hash::make($input['new_password']);
        $user->save();

        return response()->json(['success' => true, 'message' => '密碼修改成功']);

    }
}
