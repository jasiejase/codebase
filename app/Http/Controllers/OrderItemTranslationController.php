<?php

namespace App\Http\Controllers;

use DB;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class OrderItemTranslationController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id;
        $headers = array();
        $rows = array();
        $items = OrderItem::where('order_page_id', $id)->get();
        $type = (!empty($items)) ? $items[0]->big_category : "";

        $headers[] = array('title'=>'項次', 'field'=>'row_number', 'align'=>'center', 'sortable'=>true, 'width'=>30);
        $headers[] = array('title'=>'品項', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('field'=> 'progress', 'title'=> '進度', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'progressFormatter', 'printIgnore'=> 'true');
        $headers[] = array('title'=>'類型', 'field'=>'middle_category', 'align'=>'center', 'sortable'=>true);
        if ("BS" == $type)
        {
            $headers[] = array('title'=>'門寬', 'field'=>'width', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'門高', 'field'=>'total_height', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'門厚', 'field'=>'up_window', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'SKIN', 'field'=>'skin', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'SKIN顏色', 'field'=>'colour', 'align'=>'center', 'sortable'=>true);
        }
        else
        {
            $headers[] = array('title'=>'系列', 'field'=>'category', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'框寬', 'field'=>'width', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'框高', 'field'=>'total_height', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'框深', 'field'=>'up_window', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'門檻', 'field'=>'threshold', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'埋入', 'field'=>'depth', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'顏色', 'field'=>'colour', 'align'=>'center', 'sortable'=>true);
        }
        $headers[] = array('title'=>'開向', 'field'=>'direction', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'數量', 'field'=>'amount', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'已出貨', 'field'=>'shipped', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'材質', 'field'=>'material', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'鎖', 'field'=>'lock', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'鎖高', 'field'=>'lock_height', 'align'=>'left', 'sortable'=>true);
        if ("BS" == $type)
        {
            $headers[] = array('title'=>'把手', 'field'=>'handle', 'align'=>'center', 'sortable'=>true);
        }
        $headers[] = array('title'=>'鉸鏈', 'field'=>'hinge', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'防橇栓', 'field'=>'bolt', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'上加工', 'field'=>'up_box', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'下加工', 'field'=>'down_box', 'align'=>'center', 'sortable'=>true);
        if ("BS" == $type)
        {
            $headers[] = array('title'=>'玻璃', 'field'=>'glass', 'align'=>'center', 'sortable'=>true);
        }
        else
        {
            $headers[] = array('title'=>'氣密條', 'field'=>'airtight_strip', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'防煙條', 'field'=>'smoke_strip', 'align'=>'center', 'sortable'=>true);
            $headers[] = array('title'=>'補強鐵', 'field'=>'reinforce_iron', 'align'=>'center', 'sortable'=>true);
        }
        $headers[] = array('title'=>'單價', 'field'=>'price', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'總價', 'field'=>'total', 'align'=>'center', 'sortable'=>true);

        foreach ($items as $item)
        {
            if ($item->big_category == "BS")
            {
                $rows[] = $this->doorTranslation($item);
            }
            else
            {
                $rows[] = $this->frameTranslation($item);
            }
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $rows
        ));
    }

    // Translate 南亞一個項次
    private function doorTranslation($item)
    {
        $doorType = substr($item->window, 0, 1);
        $translations = array();

        $translations['row_number'] = $item->row_number;
        $translations['name'] = $item->name;
        $translations['progress'] = $item->progress;
        $translations['middle_category'] = $this->getTranslation('ny_middle_categories', $item->middle_category) . " " . $this->getTranslation('ny_door_types', $doorType);
        $translations['direction'] = $this->getTranslation('ny_directions', substr($item->window, 1, 2));
        $translations['width'] = $item->width;
        $translations['up_window'] = $item->up_window;
        $translations['total_height'] = $item->total_height;
        $translations['skin'] = $this->getSkin($doorType, $item->wind, $item->colour);
        $translations['colour'] = $this->getDoorColour($doorType, $item->colour);
        $translations['amount'] = $item->amount;
        $translations['shipped'] = $item->shipped;
        $translations['price'] = $item->price;
        $translations['total'] = $item->amount * $item->price;
        $translations['material'] = (substr($item->mixed_frame, 0, 1) == "T") ? "SECC 1.2t" : "SUS 1.2t";
        $translations['hinge'] = substr($item->mixed_frame, 2, 1) . " X " . $this->getTranslation('ny_hinges', substr($item->mixed_frame, 1, 1));
        $translations['bolt'] = substr($item->mixed_frame, 4, 1) . " X " . $this->getTranslation('ny_bolts', substr($item->mixed_frame, 3, 1));
        $translations['up_box'] = $this->getTranslation('ny_up_boxes', substr($item->box, 1, 1));
        $translations['down_box'] = $this->getTranslation('ny_down_boxes', substr($item->box,-1));
        $translations['glass'] = $this->getGlass($item->L1, $item->L2, $item->L3, $item->L4, $item->L5);
        $translations['lock'] = $this->getTranslation('ny_hardware', $item->lock);
        $translations['lock_height'] = $item->lock_height;
        $translations['handle'] = $this->getHandle($item->f1);

        return $translations;
    }

    private function frameTranslation($item)
    {
        $translations['row_number'] = $item->row_number;
        $translations['name'] = $item->name;
        $translations['progress'] = $item->progress;
        $translations['middle_category'] = $this->getTranslation('ny_frame_types', $item->wind) . $this->getTranslation('ny_middle_categories', $item->middle_category) ;
        $translations['category'] = $this->getTranslation('ny_door_types', substr($item->window, 0, 1));
        $translations['width'] = $item->width;
        $translations['total_height'] = $item->total_height;
        $translations['up_window'] = $item->up_window;
        $translations['depth'] = $item->depth;
        $translations['direction'] = $this->getTranslation('ny_directions', substr($item->window, 1, 2));
        $translations['material'] = (substr($item->window, 1, 1) == "S") ? "SUS 1.5t" : "SECC 1.6t";
        $translations['threshold'] = $item->threshold;
        $translations['colour'] = $item->colour;
        $translations['amount'] = $item->amount;
        $translations['shipped'] = $item->shipped;
        $translations['price'] = $item->price;
        $translations['total'] = $item->amount * $item->price;
        $translations['hinge'] = substr($item->mixed_frame, 2, 1) . " X " . $this->getTranslation('ny_hinges', substr($item->mixed_frame, 1, 1));
        $translations['bolt'] = substr($item->mixed_frame, 4, 1) . " X " . $this->getTranslation('ny_bolts', substr($item->mixed_frame, 3, 1));
        $translations['up_box'] = $this->getTranslation('ny_up_boxes', substr($item->box, 1, 1));
        $translations['down_box'] = $this->getTranslation('ny_down_boxes', substr($item->box,-1));
        $translations['lock'] = $this->getTranslation('ny_hardware', $item->lock);
        $translations['lock_height'] = $item->lock_height;
        $translations['smoke_strip'] = $item->smoke_strip;
        $translations['reinforce_iron'] = $item->reinforce_iron;
        $translations['airtight_strip'] = $item->airtight_strip;

        return $translations;
    }

    private function getSkin($doorType, $wind, $colour)
    {
        $skinType = $this->getTranslation('ny_colours', $colour);
        switch ($doorType)
        {
            case "G":
                return $wind . "P" . $skinType;
            case "T":
                return "3mm " . $skinType . " (樣式 $wind)" ;
            case "P":
                return $wind . "P" . $skinType;
            case "C":
                return "1KP330<BR>71P300<BR>鑄鋁" . $wind;
            case "F":
                return "看設計圖";
            default:
                return "找不到";
        }
    }

    private function getDoorColour($doorType, $colour)
    {
        if ($doorType == "C")
        {
            $colours = array();

            switch (substr($colour, 0, 1))
            {
                case "3": $colours[] = "3GC"; break;
                case "6": $colours[] = "6KF"; break;
                case "H": $colours[] = "0HS"; break;
                case "D": $colours[] = "0DO"; break;
                default: $colours[] = "找不到";
            }

            switch (substr($colour, 2, 1))
            {
                case "Y": $colours[] = "3YF"; break;
                case "K": $colours[] = "3KF"; break;
                case "L": $colours[] = "3LF"; break;
                case "M": $colours[] = "3MF"; break;
                case "G": $colours[] = "3GF"; break;
                default: $colours[] = "找不到";
            }

            switch (substr($colour, 1, 1))
            {
                case "R": $colours[] = "紅銅沙"; break;
                case "K": $colours[] = "黃銅沙"; break;
                case "E": $colours[] = "香檳銀"; break;
                default: $colours[] = "找不到";
            }

            return implode("<BR>", $colours);
        }
        return $colour;
    }

    private function getGlass($L1, $L2, $L3, $L4, $L5)
    {
        if ($L1 == "0" || $L1 == "")
        {
            return "";
        }
        else if ($L3 == "0" || $L3 == "")
        {
            return "貓眼: 位置 從上$L1 從鎖邊$L2";
        }
        else
        {
            return $L3 . "x" . "$L4" . "x" . $L5 . "mm 位置 從上$L1, 從鎖邊$L2";
        }
    }

    private function getHandle($criteria)
    {
        if ("" == $criteria || "0" == $criteria) return "";

        $code_and_height = str_replace("SK", "", strtoupper($criteria));
        $code = substr($code_and_height, 0, 3);
        $height = preg_replace("/^$code/", "", $code_and_height);
        $handle = $this->getTranslation('ny_hardware', "SK" . $code);

        return $handle . " / " . $height;
    }

    private function getTranslation($table, $criteria)
    {
        $result = DB::table($table)->where('code', $criteria)->first();

        return (!is_null($result)) ? $result->translation : "找不到";
    }
}
