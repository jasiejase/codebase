<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Corporation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporationOptions = Corporation::all()->pluck('id','name')->toArray();

        $modal = array();
        $modal[] = array('type'=>'hidden', 'name'=>'index');
        $modal[] = array('type'=>'hidden', 'name'=>'id');
        $modal[] = array('type'=>'text', 'text'=>'客戶', 'name'=>'company', 'disabled'=>'disabled', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'統編', 'name'=>'gui', 'disabled'=>'disabled', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'地址', 'name'=>'address', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'電話', 'name'=>'phone', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'電話2', 'name'=>'phone2');
        $modal[] = array('type'=>'text', 'text'=>'傳真', 'name'=>'fax');
        $modal[] = array('type'=>'text', 'text'=>'網站', 'name'=>'website');
        $modal[] = array('type'=>'select', 'text'=>'集團', 'name'=>'corporations[]', 'id'=>'corp_id', 'value'=>'', 'validation'=>'required', 'disabled'=>'multiple', 'options'=> $corporationOptions);

        $data['modal'] = $modal;
        $data['title'] = "客戶基本資料";

        return view('company.index', $data);
    }

    public function show()
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center', 'printIgnore'=>true);
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center', 'printIgnore'=>true);
        $headers[] = array('title'=>'公司', 'field'=>'company', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'統編', 'field'=>'gui', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'地址', 'field'=>'address', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'電話', 'field'=>'phone', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'電話2', 'field'=>'phone2', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'傳真', 'field'=>'fax', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'網站', 'field'=>'website', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'集團', 'field'=>'corp', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'min-width'=>90, 'events'=>'buttonOperation', 'formatter'=>'buttonColumn', 'clickToSelect'=>false, 'printIgnore'=>true);

        $companies = Company::with('corporations')->where('type', 'customer')->get();
        $companies->each(function ($item, $key) {
            $item['corp'] = $item->corporations->pluck('name')->join('<br>');
            $item['corp_id'] = $item->corporations->pluck('id');
        });

        return Response::json(array(
            'header' => $headers,
            'row' => $companies
        ));
    }

    public function create(Request $request)
    {
        $validatedData = $this->validator($request);

        $company = new Company;
        $company->company = $request->company;
        $company->gui = $request->gui;
        $company->address = $request->address;
        $company->phone = $request->phone;
        $company->phone2 = $request->phone2 ?? '';
        $company->fax = $request->fax ?? '';
        $company->website = $request->website ?? '';
        $company->type = 'customer';
        $company->save();

        $corporation = new Corporation;
        $corporation->name = $request->company;
        $corporation->save();

        $company->corporations()->attach($request->corp_id);
        $company->corporations()->attach($corporation->id);

        return Response::json(array(
            'id' => $company->id,
            'corp_id_id' => $corporation->id,
            'corp_id_name' => $company->company
        ));
    }

    public function update(Request $request)
    {
        $validatedData = $this->validator($request);

        $company = Company::find($request->id);
        $company->address = $request->address;
        $company->phone = $request->phone;
        $company->phone2 = $request->phone2 ?? '';
        $company->fax = $request->fax ?? '';
        $company->website = $request->website ?? '';
        $company->save();
        $company->corporations()->sync($request->corp_id);

        return Response::json(array(
            'success' => 1
        ));
    }

    private function validator(Request $request)
    {
        $validator = $this->validate($request, [
            'company' => 'required',
            'gui' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        return $validator;
    }
}
