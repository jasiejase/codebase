<?php

namespace App\Http\Controllers;

use App\Models\MaterialSupplier;
use Illuminate\Http\Request;

class MaterialSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MaterialSupplier  $materialSupplier
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialSupplier $materialSupplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialSupplier  $materialSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialSupplier $materialSupplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MaterialSupplier  $materialSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialSupplier $materialSupplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialSupplier  $materialSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialSupplier $materialSupplier)
    {
        //
    }
}
