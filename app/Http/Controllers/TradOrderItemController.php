<?php

namespace App\Http\Controllers;

use App\Models\TradOrderItem;
use Illuminate\Http\Request;

class TradOrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TradOrderItem  $tradOrderItem
     * @return \Illuminate\Http\Response
     */
    public function show(TradOrderItem $tradOrderItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TradOrderItem  $tradOrderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(TradOrderItem $tradOrderItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TradOrderItem  $tradOrderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TradOrderItem $tradOrderItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TradOrderItem  $tradOrderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(TradOrderItem $tradOrderItem)
    {
        //
    }
}
