<?php

namespace App\Http\Controllers;

use DB;
use App\Models\OrderItem;
use App\Models\orderItemHistory;
use App\Models\OrderPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class OrderItemController extends Controller
{
    // Used in 南亞 Orders
    public function index(Request $request)
    {
        $headers = array();
        $headers[] = array('field'=> 'state', 'checkbox'=> true, 'align'=> 'center', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'id', 'title'=> 'ID', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'row_number', 'title'=> "項次", 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'name', 'title'=> '品項', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'progressFormatter', 'width'=> 100);
        $headers[] = array('field'=> 'progress', 'title'=> '進度', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'progressFormatter', 'printIgnore'=> 'true');
        $headers[] = array('field'=> 'big_category', 'title'=> '大類', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'middle_category', 'title'=> '中類', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'window', 'title'=> '上下中窗', 'sortable'=> true, 'align'=> 'center', 'formatter'=> 'nyWindowFormatter');
        $headers[] = array('field'=> 'width', 'title'=> '寬度', 'sortable'=> true, 'align'=> 'right');
        $headers[] = array('field'=> 'up_window', 'title'=> '上窗高度', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'total_height', 'title'=> '總高度', 'sortable'=> true, 'align'=> 'right');
        $headers[] = array('field'=> 'wind', 'title'=> '風壓', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'colour', 'title'=> '色號', 'sortable'=> true, 'align'=> 'center', 'footerFormatter'=> 'setTotalText');
        $headers[] = array('field'=> 'amount', 'title'=> '數量', 'sortable'=> true, 'align'=> 'right', 'footerFormatter'=> 'getColumnSum', 'formatter'=> 'progressFormatter');
        $headers[] = array('field'=> 'shipped', 'title'=> '已出貨數量', 'sortable'=> true, 'align'=> 'right', 'footerFormatter'=> 'getColumnSum', 'formatter'=> 'progressFormatter');
        $headers[] = array('field'=> 'threshold', 'title'=> '門框總類', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'mixed_frame', 'title'=> '混合框', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'box', 'title'=> 'BOX色號', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'depth', 'title'=> '埋入深度', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'lock', 'title'=> '鎖', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'lock_height', 'title'=> '鎖高', 'sortable'=> true, 'align'=> 'right');
        $headers[] = array('field'=> 'L1', 'title'=> 'L1', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'L2', 'title'=> 'L2', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'L3', 'title'=> 'L3', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'L4', 'title'=> 'L4', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'L5', 'title'=> 'L5', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'f1', 'title'=> '框型一', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'f2', 'title'=> '框型二', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'f3', 'title'=> '框型三', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'f4', 'title'=> '框型四', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'reinforce_iron', 'title'=> '補強鐵', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'airtight_strip', 'title'=> '氣密條', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'smoke_strip', 'title'=> '遮煙條', 'sortable'=> true, 'align'=> 'center');
        $headers[] = array('field'=> 'due_date', 'title'=> '交期', 'sortable'=> true, 'align'=> 'center', 'printIgnore'=> 'true');
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', ' width'=> '90px', 'formatter'=> 'itemButtonColumn', 'events'=> 'itemButtonOperation', 'clickToSelect'=> false, 'printIgnore'=> 'true');

        $items = OrderItem::where('order_page_id', $request->id)->get();

        return Response::json(array(
            'header' => $headers,
            'row' => $items
        ));
    }

    // Insert / Update 南亞訂單
    public function update(Request $request)
    {
        if (0 == $request->order_item_id) // new row
        {
            $orderItem = new OrderItem;
            $orderItem->order_page_id = $request->order_page_id;
            $orderItem->user_id = Auth::user()->id;
        }
        else
        {
            $orderItem = OrderItem::find($request->order_item_id);
        }

        $orderItem->row_number = $request->row_number;
        $orderItem->name = $request->name;
        $orderItem->progress = ($request->shipped != $request->amount) ? $request->progress : "已出貨";
        $orderItem->big_category = $request->big_category;
        $orderItem->middle_category = $request->middle_category;
        $orderItem->window = $request->window;
        $orderItem->width = $request->width;
        $orderItem->up_window = $request->up_window;
        $orderItem->total_height = $request->total_height;
        $orderItem->wind = strtoupper($request->wind);
        $orderItem->colour = $request->colour;
        $orderItem->amount = $request->amount;
        $orderItem->shipped = ($request->progress != "已出貨") ? $request->shipped : $request->amount;
        $orderItem->price = $request->price;
        $orderItem->threshold = (!is_null($request->threshold)) ? strtoupper($request->threshold) : "";
        $orderItem->mixed_frame = strtoupper($request->mixed_frame);
        $orderItem->box = strtoupper($request->box);
        $orderItem->depth = $request->depth;
        $orderItem->lock = strtoupper($request->lock);
        $orderItem->lock_height = $request->lock_height;
        $orderItem->L1 = (!is_null($request->l1)) ? $request->l1 : 0;
        $orderItem->L2 = (!is_null($request->l2)) ? $request->l2 : 0;
        $orderItem->L3 = (!is_null($request->l3)) ? $request->l3 : 0;
        $orderItem->L4 = (!is_null($request->l4)) ? $request->l4 : 0;
        $orderItem->L5 = (!is_null($request->l5)) ? $request->l5 : 0;
        $orderItem->f1 = (!is_null($request->f1)) ? $request->f1 : "";
        $orderItem->f2 = (!is_null($request->f2)) ? $request->f2 : "";
        $orderItem->f3 = (!is_null($request->f3)) ? $request->f3 : "";
        $orderItem->f4 = (!is_null($request->f4)) ? $request->f4 : "";
        $orderItem->reinforce_iron = (!is_null($request->reinforce_iron)) ? $request->reinforce_iron : "";
        $orderItem->airtight_strip = (!is_null($request->airtight_strip)) ? $request->airtight_strip : "";
        $orderItem->smoke_strip = (!is_null($request->smoke_strip)) ? $request->smoke_strip : "";
        $orderItem->due_date = $request->due_date;

        $orderItem->save();

        // Update orderPage Progress
        $orderPageProgress = "";
        $progress = OrderItem::where('order_page_id', $request->order_page_id)->pluck('progress')->unique();
        $orderPage = OrderPage::find($request->order_page_id);

        if ($progress->count() > 1)
        {
            $orderPage->progress = "<abbr title='" . $progress->implode(', ') . "'>" . $progress->count() . "種進度</abbr>";
            $orderPageProgress = "<abbr title='" . $progress->implode(', ') . "'>" . $progress->count() . "種進度</abbr>";
        }
        else if ($progress->count() == 1)
        {
            $orderPage->progress = $progress[0];
            $orderPageProgress = $progress[0];
        }
        else
        {
            $orderPageProgress = "收到進度";
        }

        // Update Order Page changable column
        // Make sure user cannot change order type anymore
        $orderPage->changeable = 0;
        $orderPage->save();

        if (0 == $request->order_item_id)
        {
            $orderItemHistory = new orderItemHistory;
            $orderItemHistory->order_item_id = $orderItem->id;
            $orderItemHistory->user_id = Auth::user()->id;
            $orderItemHistory->field = "progress";
            $orderItemHistory->old_value = "";
            $orderItemHistory->new_value = "收到訂單";
            $orderItemHistory->save();
        }
        return array('id'=>$orderItem->id, 'text'=>'success', 'progress' => $orderPageProgress, 'changeable' => 0);
    }
}
