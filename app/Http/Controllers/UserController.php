<?php

namespace App\Http\Controllers;

use Notification;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Corporation;
use App\Notifications\UserUpdated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporationOptions = Corporation::all()->pluck('id','name')->toArray();
        $isActiveOptions = array();
        $isActiveOptions["活躍"] = "活躍";
        $isActiveOptions["停用"] = "停用";

        $modal = array();
        $modal[] = array('type'=>'hidden', 'name'=>'index');
        $modal[] = array('type'=>'hidden', 'name'=>'id');
        $modal[] = array('type'=>'text', 'text'=>'姓名', 'name'=>'name', 'disabled'=>'disabled');
        $modal[] = array('type'=>'text', 'text'=>'手機', 'name'=>'mobile', 'disabled'=>'disabled');
        $modal[] = array('type'=>'text', 'text'=>'Email', 'name'=>'email', 'disabled'=>'disabled');
        $modal[] = array('type'=>'text', 'text'=>'職位', 'name'=>'position', 'validation'=>'required', 'disabled'=>'');
        $modal[] = array('type'=>'select', 'text'=>'集團', 'name'=>'corporations[]', 'id'=>'corp_id', 'validation'=>'required', 'disabled'=>'multiple', 'options'=> $corporationOptions);
        $modal[] = array('type'=>'select', 'text'=>'帳戶', 'name'=>'is_active', 'validation'=>'required', 'disabled'=>'', 'options'=> $isActiveOptions);

        $data['modal'] = $modal;
        $data['title'] = "用戶資料";

        return view('user.index', $data);
    }

    public function show()
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center', 'printIgnore'=>true);
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center', 'printIgnore'=>true);
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'姓名', 'field'=>'name', 'align'=>'center', 'sortable'=>true, 'footerFormatter' => "getTotalText");
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'職位', 'field'=>'position', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'手機', 'field'=>'mobile', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'Email', 'field'=>'email', 'align'=>'left', 'sortable'=>true);
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'集團', 'field'=>'corp', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('formatter'=>'customFormatter', 'title'=>'用戶狀態', 'field'=>'is_active', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'events'=>'buttonOperation', 'formatter'=>'buttonColumn', 'clickToSelect'=>false, 'printIgnore'=>true);

        $users = User::with('corporations')->get();

        $users->each(function ($item, $key) {
            $item['corp'] = $item->corporations->pluck('name')->join('<br>');
            $item['corp_id'] = $item->corporations->pluck('id');
        });

        return Response::json(array(
            'header' => $headers,
            'row' => $users
        ));
    }

    public function update(Request $request)
    {
        $data = $request->post();

        $user = User::find($request->id);
        $user->position = $request->position;
        $user->is_active = $request->is_active;
        $user->save();
        $user->corporations()->sync($request->corp_id);

        // Notify 集團 users that user is_active status changed
        if ($user->wasChanged('is_active'))
        {
            $corporationIds = $user->corporations->pluck('id');
            $toUser = Corporation::with('users')->whereIn('id', $corporationIds)->get();
            $allUsers = $toUser->pluck('users')->collapse()->unique('id');

            $details = [
                'user' => $user->name,
                'data' => $data
            ];

            Notification::send($allUsers, new UserUpdated($details));
        }

        return Response::json(array('success' => 1));
    }

    public function markAsReadNotifications()
    {
        Auth::user()->unreadNotifications->markAsRead();

        return redirect()->back();
    }
}
