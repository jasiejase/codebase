<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Corporation;
use App\Models\Project;
use App\Models\User;
use Response;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporations = User::find(Auth::user()->id)->corporations->pluck("id");
        $companies = Corporation::with(['companies' => function ($query) {
                                        $query->where('type', '!=', 'supplier');
                                    }])->whereIn('id', $corporations)->orderBy('id', 'desc')->get();

        $projectModal = array();
        $projectModal[] = array('type'=>'text', 'text'=>'建案', 'name'=>'site', 'validation'=>'required');
        $projectModal[] = array('type'=>'text', 'text'=>'地址', 'name'=>'address', 'validation'=>'');
        $projectModal[] = array('type'=>'text', 'text'=>'工地聯繫人', 'name'=>'site_contact', 'validation'=>'');
        $projectModal[] = array('type'=>'text', 'text'=>'工地電話', 'name'=>'site_phone', 'validation'=>'');
        $projectModal[] = array('type'=>'select', 'text'=>'建案負責人', 'name'=>'contacts[]', 'id'=>'contacts', 'value'=>'', 'disabled'=>'multiple', 'options'=> array());
        $projectModal[] = array('type'=>'textarea', 'text'=>'備註', 'name'=>'note', 'validation'=>'');

        $standards = array('CNS 11227-1'=>'CNS 11227-1', 'CNS 11227'=>'CNS 11227', '非防火'=>'非防火');
        $type = array('南亞'=>'南亞','鐵門'=>'鐵門','門檻'=>'門檻');
        $orderPageModal = array();
        $orderPageModal[] = array('type'=>'hidden', 'name'=>'index', 'id'=>'index');
        $orderPageModal[] = array('type'=>'hidden', 'name'=>'id', 'id'=>'id');
        $orderPageModal[] = array('type'=>'select', 'text'=>'報告標準', 'name'=>'standard', 'validation'=>'required', 'disabled'=>'', 'options'=> $standards);
        $orderPageModal[] = array('type'=>'select', 'text'=>'訂單類型', 'name'=>'type', 'validation'=>'required', 'disabled'=>'', 'options'=> $type);
        $orderPageModal[] = array('type'=>'text', 'text'=>'訂單編號', 'name'=>'order_number', 'validation'=>'required');
        $orderPageModal[] = array('type'=>'text', 'text'=>'備註', 'name'=>'note');
        $orderPageModal[] = array('type'=>'date', 'text'=>'客戶需求日', 'name'=>'customer_due_date', 'validation'=>'');
        $orderPageModal[] = array('type'=>'date', 'text'=>'收到日期', 'name'=>'received_date', 'validation'=>'required');

        $data['companies'] = $companies;
        $data['title'] = "建案資料";
        $data['projectModal'] = $projectModal;
        $data['orderPageModal'] = $orderPageModal;

        return view('project.index', $data);
    }

    /**
     * Ajax
     * Get Company Info
     */

    public function getCompanyInfo(Request $request)
    {
        $extra = array(); // extra contact used in 英聖 n 泰慶 集團 coz most contacts are employees in this two company
        $id = $request->id;
        $company = Company::with('contacts')->find($id);
        $userCorporations = User::find(Auth::user()->id)->corporations->pluck('pivot')->pluck('corporation_id')->toArray();
        $companyCorporations = Company::find($id)->corporations->pluck('pivot')->pluck('corporation_id')->toArray();
        $corporations = array(1,2);

        if (!empty(array_intersect($corporations, $userCorporations)))
        {
            // if (in_array(1, $companyCorporations) && $id != 1)
            // {
            //     $extra = Contact::where('company_id', 1)->get();
            // }
            // else
            if (in_array(2, $companyCorporations) && $id != 2)
            {
                $extra = Contact::where('company_id', 2)->get();
            }
        }

        return Response::json(array(
            'company' => $company,
            'extra' => $extra
        ));
    }

    /**
     * Ajax
     * Get Project List
     */
    public function getProjectList(Request $request)
    {
        $id = $request->id;
        $projects = Project::where('company_id', $id)->select('id', 'site')->orderBy('id', 'desc')->get();

        return Response::json(array(
            'projects' => $projects
        ));
    }

    /**
     * Ajax
     * Get Project Information
     */
    public function getProjectInfo(Request $request)
    {
        $id = $request->id;
        $project = Project::with('contacts')->find($id);

        return Response::json(array(
            "project" => $project
        ));
    }

    /**
     * Ajax
     * Edit Project (including insert new project)
     */
    public function editProject(Request $request)
    {
        $project = ($request->id != "") ? Project::find($request->id) : new Project;
        $project->company_id = $request->company_id;
        $project->site = $request->site;
        $project->address = $request->address ?? '';
        $project->site_contact = $request->site_contact ?? '';
        $project->site_phone = $request->site_phone ?? '';
        $project->note = $request->note ?? '';
        $project->save();
        $project->contacts()->sync($request->contacts);

        return response()->json([
            'id' => $project->id
        ]);
    }
}
