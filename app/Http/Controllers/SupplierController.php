<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Models\Material;
use App\Models\MaterialSupplier;
use App\Models\Quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class SupplierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialOptions = Material::all()->pluck('id','name')->toArray();

        $modal = array();
        $modal[] = array('type'=>'hidden', 'name'=>'index');
        $modal[] = array('type'=>'hidden', 'name'=>'id');
        $modal[] = array('type'=>'text', 'text'=>'供應商', 'name'=>'supplier', 'disabled'=>'', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'統編', 'name'=>'gui', 'disabled'=>'disabled', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'地址', 'name'=>'address', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'電話', 'name'=>'phone', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'電話2', 'name'=>'phone2');
        $modal[] = array('type'=>'text', 'text'=>'傳真', 'name'=>'fax');
        $modal[] = array('type'=>'text', 'text'=>'網站', 'name'=>'website');
        $modal[] = array('type'=>'text', 'text'=>'Email', 'name'=>'email');
        $modal[] = array('type'=>'select', 'text'=>'供應項目', 'name'=>'materials[]', 'id'=>'mat_id', 'value'=>'', 'validation'=>'required', 'disabled'=>'multiple', 'options'=> $materialOptions);

        $data['modal'] = $modal;
        $data['title'] = "供應商基本資料";

        return view('supplier.index', $data);
    }

    /**
     * Show list of supplier companies
     */
    public function show()
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center');
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center');
        $headers[] = array('title'=>'供應商', 'field'=>'supplier', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'統編', 'field'=>'gui', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'地址', 'field'=>'address', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'電話', 'field'=>'phone', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'電話2', 'field'=>'phone2', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'傳真', 'field'=>'fax', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'網站', 'field'=>'website', 'align'=>'left', 'sortable'=>true);
        $headers[] = array('title'=>'Email', 'field'=>'email', 'align'=>'left', 'sortable'=>true);
        $headers[] = array('title'=>'供應項目', 'field'=>'mat', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'min-width'=>90, 'events'=>'window.operateEvents', 'formatter'=>'showOperateColumn', 'clickToSelect'=>false);

        $suppliers = Supplier::with('materials')->get();
        $suppliers->each(function ($item, $key) {
            $filtered = $item->materials->filter(function ($v, $k) {
                return $v->available == 1;
            });

            $item['mat'] = $filtered->pluck('material')->pluck('name')->sort()->join(', ');
            $item['mat_id'] = $filtered->pluck('material')->pluck('id');
        });

        return Response::json(array(
            'header' => $headers,
            'row' => $suppliers
        ));
    }

    /**
     * Add new supplier
     */
    public function create(Request $request)
    {
        $validatedData = $this->validator($request);

        $supplier = new Supplier;
        $supplier->supplier = $request->supplier;
        $supplier->gui = $request->gui;
        $supplier->address = $request->address;
        $supplier->phone = $request->phone;
        $supplier->phone2 = $request->phone2 ?? '';
        $supplier->fax = $request->fax ?? '';
        $supplier->website = $request->website ?? '';
        $supplier->email = $request->email ?? '';
        $supplier->save();

        foreach ($request->mat_id as $mid) {
            $ms = new MaterialSupplier;
            $ms->supplier_id = $supplier->id;
            $ms->material_id = $mid;
            $ms->available   = true;
            $ms->save();
        }

        return Response::json(array(
            'id' => $supplier->id
        ));
    }

    public function update(Request $request)
    {
        $validatedData = $this->validator($request);
  
        $supplier = Supplier::find($request->id);
        $supplier->address = $request->address;
        $supplier->phone = $request->phone;
        $supplier->phone2 = $request->phone2 ?? '';
        $supplier->fax = $request->fax ?? '';
        $supplier->website = $request->website ?? '';
        $supplier->email = $request->email ?? '';
        $supplier->save();

        $deleteMaterial = DB::table('material_suppliers')
                            ->where('supplier_id', $request->id)
                            ->whereNotIn('material_id', $request->mat_id)
                            ->update(['available' => 0]);

        foreach ($request->mat_id as $mid) {
            $exist = MaterialSupplier::where('supplier_id', $request->id)->where('material_id', $mid)->get();

            if (!$exist->isEmpty()) {
                $ms = MaterialSupplier::find($exist[0]->id);
                $ms->available = 1;
                $ms->save();
            } else {
                $ms = new MaterialSupplier;
                $ms->supplier_id = $request->id;
                $ms->material_id = $mid;
                $ms->available   = 1;
                $ms->save();
            }
        }

        return Response::json(array(
            'success' => 1
        ));
    }

    private function validator(Request $request)
    {
        $validator = $this->validate($request, [
            'supplier' => 'required',
            'gui' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        return $validator;
    }

    public function viewMaterial(Request $request)
    {
        $headers = array();
        $headers[] = array('title'=>'quote ID', 'field'=>'id', 'align'=>'center');
        $headers[] = array('title'=>'material ID', 'field'=>'material_id', 'align'=>'center');
        $headers[] = array('title'=>'類型', 'field'=>'category', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'品項', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'規格', 'field'=>'size', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'單位', 'field'=>'unit', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'價格', 'field'=>'price', 'align'=>'right', 'sortable'=>true, 'formatter'=>'priceFormatter');
        $headers[] = array('title'=>'報價日', 'field'=>'date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'供應', 'field'=>'available', 'align'=>'center', 'sortable'=>true, 'formatter'=>'availableFormatter');
        $headers[] = array('title'=>'備註', 'field'=>'note', 'align'=>'center', 'sortable'=>true);

        $sql = "SELECT q.id, m.id AS 'material_id', ms.available, m.category, m.name, m.size, m.unit, q.price, q.date, q.note
                FROM materials m INNER JOIN material_suppliers ms ON m.id = ms.material_id
                    INNER JOIN quotes q ON ms.id = q.material_supplier_id
                WHERE ms.supplier_id = $request->id
                ORDER BY q.date DESC";
        $result = DB::select($sql);

        $data = array();
        foreach ($result as $row) {
            $data[] = (array) $row;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $data
        ));
    }

    public function viewShipment(Request $request)
    {
        $headers = array();
        $headers[] = array('title'=>'廠長', 'field'=>'approved_by_1', 'align'=>'center', 'sortable'=>true, 'formatter'=>'approvedFormatter');
        $headers[] = array('title'=>'Jason', 'field'=>'approved_by_2', 'align'=>'center', 'sortable'=>true, 'formatter'=>'approvedFormatter');
        $headers[] = array('title'=>'品名', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'單位', 'field'=>'unit', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'價格', 'field'=>'price', 'align'=>'right', 'sortable'=>true);
        $headers[] = array('title'=>'數量', 'field'=>'amount', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'下單日', 'field'=>'order_date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'收貨日', 'field'=>'received_at', 'align'=>'center', 'sortable'=>true, 'formatter'=>'receivedAtFormatter');

        $sql = "SELECT approved_by_1, approved_by_2, unit, name, price, amount, received_at, s.order_date
                FROM shipments s INNER JOIN material_shipment ms ON s.id = ms.shipment_id
                    INNER JOIN materials m ON m.id = ms.material_id
                WHERE s.supplier_id = $request->id
                ORDER BY s.id DESC";
        $result = DB::select($sql);

        $data = array();
        foreach ($result as $row) {
            $data[] = (array) $row;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $data
        ));
    }
}
