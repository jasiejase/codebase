<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Models\Material;
use App\Models\MaterialSupplier;
use App\Models\Quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplierOption = Supplier::all()->pluck('id','supplier')->toArray();

        $modal = array();
        $modal[] = array('type'=>'hidden', 'name'=>'index');
        $modal[] = array('type'=>'hidden', 'name'=>'id');
        $modal[] = array('type'=>'text', 'text'=>'類別', 'name'=>'category', 'disabled'=>'', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'品項', 'name'=>'name', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'規格', 'name'=>'size');
        $modal[] = array('type'=>'text', 'text'=>'單位', 'name'=>'unit', 'validation'=>'required');
        $modal[] = array('type'=>'text', 'text'=>'庫存線', 'name'=>'undersupply');
        $modal[] = array('type'=>'text', 'text'=>'備註', 'name'=>'note');
        $modal[] = array('type'=>'select', 'text'=>'供應商', 'name'=>'suppliers[]', 'id'=>'sup_id', 'value'=>'', 'validation'=>'required', 'disabled'=>'multiple', 'options'=> $supplierOption);

        $data['modal'] = $modal;
        $data['title'] = "原物料基本資料";

        return view('material.index', $data);
    }

    /**
     * Show list of supplier companies
     */
    public function show()
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center');
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center');
        $headers[] = array('title'=>'類別', 'field'=>'category', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'品項', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'規格', 'field'=>'size', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'單位', 'field'=>'unit', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'庫存線', 'field'=>'undersupply', 'align'=>'right', 'sortable'=>true);
        $headers[] = array('title'=>'供應商', 'field'=>'sup', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'備註', 'field'=>'note', 'align'=>'left', 'sortable'=>true);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'min-width'=>90, 'events'=>'window.operateEvents', 'formatter'=>'showOperateColumn', 'clickToSelect'=>false);

        $suppliers = Material::with('suppliers')->get();
        $suppliers->each(function ($item, $key) {
            $item['sup'] = $item->suppliers->where('available', 1)->pluck('supplier')->pluck('supplier')->sort()->join(', ');
            $item['sup_id'] = $item->suppliers->where('available', 1)->pluck('supplier')->pluck('id');
        });
        
        return Response::json(array(
            'header' => $headers,
            'row' => $suppliers
        ));
    }

    /**
     * Add new supplier
     */
    public function create(Request $request)
    {
        $validatedData = $this->validator($request);

        $material = new Material;
        $material->category = $request->category;
        $material->name = $request->name;
        $material->size = $request->size ?? '';
        $material->unit = $request->unit;
        $material->undersupply = $request->undersupply ?? '';
        $material->note = $request->note ?? '';
        $material->save();

        foreach ($request->sup_id as $sid) {
            $ms = new MaterialSupplier;
            $ms->supplier_id = $sid;
            $ms->material_id = $material->id;
            $ms->available   = true;
            $ms->save();
        }

        return Response::json(array(
            'id' => $material->id
        ));
    }

    public function update(Request $request)
    {
        $validatedData = $this->validator($request);
  
        $material = Material::find($request->id);
        $material->category = $request->category;
        $material->name = $request->name;
        $material->size = $request->size ?? '';
        $material->unit = $request->unit;
        $material->undersupply = $request->undersupply ?? '';
        $material->note = $request->note ?? '';
        $material->save();

        $deleteSupplier = DB::table('material_suppliers')
                            ->where('material_id', $request->id)
                            ->whereNotIn('supplier_id', $request->sup_id)
                            ->update(['available' => 0]);

        foreach ($request->sup_id as $sid) {
            $exist = MaterialSupplier::where('material_id', $request->id)->where('supplier_id', $sid)->get();

            if (!$exist->isEmpty()) {
                $ms = MaterialSupplier::find($exist[0]->id);
                $ms->available = 1;
                $ms->save();
            } else {
                $ms = new MaterialSupplier;
                $ms->supplier_id = $sid;
                $ms->material_id = $request->id;
                $ms->available   = 1;
                $ms->save();
            }
        }

        return Response::json(array(
            'success' => 1
        ));
    }

    private function validator(Request $request)
    {
        $validator = $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
            'unit' => 'required',
            'size' => 'required'
        ]);

        return $validator;
    }

    public function viewMaterial(Request $request)
    {
        $headers = array();
        $headers[] = array('title'=>'供應商', 'field'=>'supplier', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'價格', 'field'=>'price', 'align'=>'right', 'sortable'=>true, 'formatter'=>'priceFormatter');
        $headers[] = array('title'=>'報價日', 'field'=>'date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'供應', 'field'=>'available', 'align'=>'center', 'sortable'=>true, 'formatter'=>'availableFormatter');
        $headers[] = array('title'=>'備註', 'field'=>'note', 'align'=>'center', 'sortable'=>true);

        $sql = "SELECT s.supplier, ms.available, q.price, q.date, q.note
                FROM materials m INNER JOIN material_suppliers ms ON m.id = ms.material_id
                    INNER JOIN suppliers s ON ms.supplier_id = s.id
                    INNER JOIN quotes q ON q.material_supplier_id = ms.id
                WHERE m.id = $request->id
                ORDER BY q.date DESC";
        $result = DB::select($sql);

        $data = array();
        foreach ($result as $row) {
            $data[] = (array) $row;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $data
        ));
    }

    public function viewShipment(Request $request)
    {
        $headers = array();
        $headers[] = array('title'=>'廠長', 'field'=>'approved_by_1', 'align'=>'center', 'sortable'=>true, 'formatter'=>'approvedFormatter');
        $headers[] = array('title'=>'Jason', 'field'=>'approved_by_2', 'align'=>'center', 'sortable'=>true, 'formatter'=>'approvedFormatter');
        $headers[] = array('title'=>'品名', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'單位', 'field'=>'unit', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'價格', 'field'=>'price', 'align'=>'right', 'sortable'=>true);
        $headers[] = array('title'=>'數量', 'field'=>'amount', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'下單日', 'field'=>'order_date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'收貨日', 'field'=>'received_at', 'align'=>'center', 'sortable'=>true, 'formatter'=>'receivedAtFormatter');

        $sql = "SELECT approved_by_1, approved_by_2, unit, name, price, amount, order_date, received_at
                FROM shipments s INNER JOIN material_shipment ms ON s.id = ms.shipment_id
                    INNER JOIN materials m ON m.id = ms.material_id
                WHERE s.supplier_id = $request->id
                ORDER BY s.id DESC";
        $result = DB::select($sql);

        $data = array();
        foreach ($result as $row) {
            $data[] = (array) $row;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $data
        ));
    }
}
