<?php

namespace App\Http\Controllers;

use App\Models\OrderPageNote;
use App\Models\OrderPageNoteFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;

class OrderPageNoteController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id;

        $data = OrderPageNote::with('orderPageNoteFiles', 'user')->where('order_page_id', $id)->orderBy('created_at')->get();

        return Response::json($data);
    }

    public function create(Request $request)
    {
        $orderPageNote = new OrderPageNote();
        $orderPageNote->order_page_id = $request->id;
        $orderPageNote->user_id = Auth::user()->id;
        $orderPageNote->note = $request->note;
        $orderPageNote->save();

        $paths = array();
        if ($request->hasFile('newOrderPageNoteFile'))
        {
            $files = $request->file('newOrderPageNoteFile');

            foreach ($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $path = $file->StoreAs($orderPageNote->id, $filename, 's3');
                $path = Storage::disk('s3')->url($path);

                $noteFile = new OrderPageNoteFile();
                $noteFile->name = $file->getClientOriginalName();
                $noteFile->order_page_note_id = $orderPageNote->id;
                $noteFile->location = $path;
                $noteFile->mime = $file->getClientMimeType();
                $noteFile->save();

                $paths[] = array('name'=>$file->getClientOriginalName(), 'id'=>$noteFile->id);
            }
        }

        return Response::json(array(
            'paths' => $paths,
            'user' => Auth::user()->name,
            'note' => $orderPageNote->note,
            'time' => $orderPageNote->created_at
        ));
    }

    public function download(Request $request)
    {
        if (!Auth::check())
        {
            return "";
        }

        $file = OrderPageNoteFile::find($request->id);

        $headers = [
            'Content-Type' => $file->mime,
            'Content-Disposition' => 'attachment; filename="'. $file->name .'"'
        ];

        return Response::make(Storage::disk('s3')->get($file->order_page_note_id . "/" . $file->name), 200, $headers);
    }
}
