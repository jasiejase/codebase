<?php

namespace App\Http\Controllers;

use App\Models\Shipment;
use App\Models\MaterialSupplier;
use App\Models\Material;
use App\Models\Supplier;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data['title'] = "採購資料";
        return view('shipment.index', $data);
    }


    public function show()
    {
        
    }

}
