<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use App\Models\MaterialSupplier;
use App\Models\Material;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class QuoteController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $headers = array();
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center');
        $headers[] = array('title'=>'日期', 'field'=>'date', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'編號', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'供應商', 'field'=>'supplier', 'align'=>'center', 'sortable'=>true, 'width'=>200);
        $headers[] = array('title'=>'原物料', 'field'=>'material', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'單位', 'field'=>'unit', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'報價', 'field'=>'price', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'備註', 'field'=>'note', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'min-width'=>90, 'events'=>'window.operateEvents', 'formatter'=>'showOperateColumn', 'clickToSelect'=>false);

        $sql = "SELECT q.id, q.date, q.name, s.supplier, m.name AS 'material', m.unit, q.price, q.note
                FROM quotes q INNER JOIN material_suppliers ms ON q.material_supplier_id = ms.id
                    INNER JOIN materials m ON ms.material_id = m.id
                    INNER JOIN suppliers s ON ms.supplier_id = s.id";
        $result = DB::select($sql);

        $data = array();
        foreach ($result as $row) {
            $data[] = (array) $row;
        }

        return Response::json(array(
            'header' => $headers,
            'row' => $data
        ));
    }
}
