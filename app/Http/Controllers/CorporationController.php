<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Corporation;
use App\Models\User;
use Response;

class CorporationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companyOptions = Company::all()->pluck('id','company')->toArray();
        $userOptions = User::all()->pluck('id','name')->toArray();

        $modal = array();
        $modal[] = array('type'=>'hidden', 'name'=>'index');
        $modal[] = array('type'=>'hidden', 'name'=>'id');
        $modal[] = array('type'=>'text', 'text'=>'集團', 'name'=>'name', 'disabled'=>'disabled', 'validation'=>'required');
        $modal[] = array('type'=>'select', 'text'=>'附屬公司', 'name'=>'companies[]', 'id'=>'company_ids', 'validation'=>'required', 'disabled'=>'multiple', 'options'=> $companyOptions);
        $modal[] = array('type'=>'select', 'text'=>'使用者', 'name'=>'users[]', 'id'=>'user_ids', 'validation'=>'', 'disabled'=>'multiple', 'options'=> $userOptions);
        $data['modal'] = $modal;
        $data['title'] = "集團資料";

        return view('corporation.index', $data);
    }

    /**
     * Ajax Show Corporations
     */
    public function show()
    {
        $headers = array();
        $headers[] = array('field'=>'state', 'checkbox'=>true, 'align'=>'center', 'printIgnore'=>true);
        $headers[] = array('title'=>'ID', 'field'=>'id', 'align'=>'center','exportIgnore'=>true, 'printIgnore'=>true, 'visible'=>false);
        $headers[] = array('title'=>'集團', 'field'=>'name', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'公司', 'field'=>'company', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'集團使用者', 'field'=>'user', 'align'=>'center', 'sortable'=>true);
        $headers[] = array('title'=>'', 'field'=>'operate', 'align'=>'center', 'events'=>'', 'formatter'=>'', 'clickToSelect'=>false, 'printIgnore'=>true);

        $corporations = Corporation::with('users', 'companies')->get();
        $corporations->each(function ($item, $key) {
            $item['company'] = $item->companies->pluck('company')->join('<br>');
            $item['company_ids'] = $item->companies->pluck('id');
            $item['user'] = $item->users->pluck('name')->join('<br>');
            $item['user_ids'] = $item->users->pluck('id');
        });

        return Response::json(array(
            'header' => $headers,
            'row' => $corporations
        ));
    }

    /**
     * Ajax update corporation
     */
    function update(Request $request)
    {
        $corporations = Corporation::find($request->id);
        $corporations->companies()->sync($request->company_ids);
        $corporations->users()->sync($request->user_ids);

        return Response::json(array(
            'success' => 1
        ));
    }
}
