@extends('layouts.backend')

@section('css_before')
@endsection


@section('content')
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">{{ $title }}</h2>
        <div class="block">
            <div id="toolbar-table">
                <button id="new-customer" class="btn btn-outline-dark mr-5 mb-5" onclick="javascript:showTableModal('table', [], -1, $modalIds);">
                    <i class="fa fa-plus mr-5"></i>新增客戶
                </button>
            </div>
            <div class="block-content block-content-full table-responsive">
                <table id="table" class="table table-sm table-striped"></table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal For Table -->
    @component('components.modal', ['formDisplay' => $modal])
        @slot('id') table @endslot
        @slot('title') 客戶資料 @endslot

        @slot('footer')
            <button id="confirm_button" type="submit" class="btn btn-alt-success"
                    onclick="javascript:editBTableRow('table', $modalIds, '{{ route('company.create') }}', '{{ route('company.update') }}')">
                <i class="fa fa-check"></i>確認
            </button>
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
        @endslot
    @endcomponent
    <!-- End Modal For Table -->

    <!-- Modal for Contacts -->
    <div class="modal fade" id="modal-contact" tabindex="-2" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">客戶聯絡人 - <span class="modal-name"></span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>

                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-4">
                                <ul id="contact_list" class="list-group">
                                    <a href="#"><li class="list-group-item active">新增聯絡人</li></a>
                                    <a href="#"><li class="list-group-item">Jason</li></a>
                                </ul>
                            </div>

                            <div class="col-md-8">
                                <form id="form-contact">
                                    <input type="hidden" id="contact_id" name="contact_id" value="">
                                    <input type="hidden" id="company_id" name="company_id" value="">
                                    <div class="form-group">
                                        <label for="name">姓名</label>
                                        <input type="text" class="form-control" id="name" name="name" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="position">職位</label>
                                        <input type="text" class="form-control" id="position" name="position">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">電話 (含分機)</label>
                                        <input type="text" class="form-control" id="phone" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile"">手機</label>
                                        <input type="text" class="form-control" id="mobile"" name="mobile"">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="note">備註</label>
                                        <textarea class="form-control" name="note" id="note" rows="8"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button id="confirm_contact" type="submit" class="btn btn-alt-success"
                                                onclick="javascript:updateContact();">
                                            <i class="fa fa-check"></i>確認
                                        </button>
                                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <!-- End Modal for Contacts -->
@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify\bootstrap-notify.min.js') }}"></script>
<script>
    let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
    let $modalIds = {}; // cannot change name coz used in window.operateEvents in bootstrap-table
    $modalIds['id'] = new Array('index', 'id', 'company', 'gui', 'address', 'phone', 'phone2', 'fax', 'website', 'corp_id');
    $modalIds['disabled'] = new Array('company', 'gui');
    $modalIds['multiple'] = {'corp_id' : 'corp'};
    $modalIds['name'] = 'company';
    $modalIds['newName'] = '新客戶';
    $modalIds['main'] = 'company';

    createBTable('table', "{{ route('company.show') }}", sRow, false);

    var buttonOperation = {
        'click .edit': function (el, id, row, index) {
            showTableModal('table', row, index, $modalIds);
        },
        'click .contact': function (el, id, row, index) {
            $.post('/contact/index', {id: row.id}, function (result) {
                var $contactList = $('#contact_list');
                $contactList.html("");
                $contactList.append('<a href="javascript:selectContact(0, 1)"><li class="list-group-item"><i class="fa fa-plus" /> 新增聯絡人</li></a>');
                result.contacts.forEach((item, index) => {
                    $contactList.append('<a href="javascript:selectContact(' + item.id + ', ' + (index+2)  + ')"><li class="list-group-item"><i class="fa fa-user" /> ' + item.name + '</li></a>');
                });
                $('.modal-name').html(row.company);
                $('#company_id').val(row.id);
                $('#contact_list a:first-child li').trigger('click');

                $('#modal-contact').modal('show');
            });
        }
    }

    function buttonColumn() {
        return '<a class="edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;<a class="contact" href="javascript:void(0)" title="Contact"><i class="fa fa-address-book"></i></a>';
    }

    // Javascript for contact modal
    function selectContact(id, index) {
        $('.list-group-item').removeClass('active');
        $('#contact_list a:nth-child(' + index + ') li').addClass('active');
        $('#contact_id').val(id);

        if (id == 0) {
            $('#name').val('');
            $('#position').val('');
            $('#phone').val('');
            $('#mobile').val('');
            $('#email').val('');
            $('#note').val('');
            return false;
        }

        var url  = "{{ route('contact.show') }}";
        var data = { 'id' : id }
        $.post(url, data, function (result) {
            $('#name').val(result.contact.name);
            $('#position').val(result.contact.position);
            $('#phone').val(result.contact.phone);
            $('#mobile').val(result.contact.mobile);
            $('#email').val(result.contact.email);
            $('#note').val(result.contact.note);
        });
    };

    function updateContact() {
        $('#form-contact').validate({
            submitHandler: function () {
                var url  = "{{ route('contact.edit') }}";
                var data = {
                        id : $('#contact_id').val(),
                        company_id: $('#company_id').val(),
                        name: $('#name').val(),
                        position: $('#position').val(),
                        phone: $('#phone').val(),
                        mobile: $('#mobile').val(),
                        email: $('#email').val(),
                        note: $('#note').val()
                }

                $.post(url, data, function (result) {
                    var message = "";
                    var icon = "";

                    if (data.id == "0") {
                        var index = $('#contact_list a').length + 1;
                        $('#contact_list').append('<a href="javascript:selectContact(' + result.id + ', ' + index  + ')"><li class="list-group-item"><i class="fa fa-user" /> ' + data.name + '</li></a>');
                        selectContact(result.id, index);
                        message = "成功增加 " + $('.modal-name').html() + " 聯絡人 " + data.name;
                        icon = 'fa fa-user-plus';
                    } else {
                        message = data.name + "的資料修改成功";
                        icon = 'fa fa-user-edit';
                    }

                    if (result.changed) {
                        notify(message, icon, 'success', '#modal-contact');
                    }
                });
            }
        });
    }

    $('header, body').css('background-image','url("/img/75.jpg")');
    $('header').css('background-position', '-50px 0px');
</script>
@endsection
