@extends('layouts.backend')

@section('css_before')

@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Hero -->
        <div class="block block-rounded">
            <div class="block-content block-content-full bg-pattern bg-light">
                <div class="py-20 text-center">
                    <h2 class="font-w700 text-black mb-10">
                        管理帳號
                    </h2>
                    <h3 class="h5 text-muted mb-0">
                        檢視和修改帳號資料
                    </h3>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            <i class="fa fa-pencil fa-fw mr-5 text-muted"></i> 基本資料
                        </h3>
                    </div>
                    <form id="user_information">
                    <div class="block-content">
                        <div class="row items-push">
                            <div class="col-lg-3">
                                <p class="text-muted">
                                    Your account’s vital info. Your name should match your public ID.
                                </p>
                            </div>
                            <div class="col-lg-7 offset-lg-1">
                                <div class="form-group">
                                    <label for="name">姓名</label>
                                    <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="請輸入你的姓名" value="{{ Auth::user()->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="mobile"">手機號碼</label>
                                    <input type="text" class="form-control form-control-lg" id="mobile"" name="mobile" placeholder="請輸入你的手機號碼" value="{{ Auth::user()->mobile }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="email"">Email</label>
                                    <input type="email" class="form-control form-control-lg" id="email"" name="email" placeholder="請輸入你的Email" value="{{ Auth::user()->email }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="position"">職位</label>
                                    <input type="text" class="form-control form-control-lg" id="position"" name="position" value="{{ Auth::user()->position }}" disabled>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-alt-primary" onclick="javascript:updateProfile()">修改</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <form id="update_password">
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            <i class="fa fa-lock fa-fw mr-5 text-muted"></i> 修改密碼
                        </h3>
                    </div>
                    <div class="block-content">
                        <div class="row items-push">
                            <div class="col-lg-3">
                                <p class="text-muted">
                                    Keep your account as secure and as private as you like.
                                </p>
                            </div>
                            <div class="col-lg-7 offset-lg-1">
                                <div class="form-group">
                                    <label for="old_password"">舊密碼</label>
                                    <input type="password"" class="form-control form-control-lg" id="old_password"" name="old_password" placeholder="請輸入原本的密碼" required>
                                </div>
                                <div class="form-group">
                                    <label for="new_password"">新密碼</label>
                                    <input type="password" class="form-control form-control-lg" id="new_password"" name="new_password" placeholder="請輸入新密碼" required>
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password"">確認新密碼</label>
                                    <input type="password" class="form-control form-control-lg" id="confirm_password"" name="confirm_password" placeholder="請重新輸入新密碼" required>
                                </div>
                                <div class="form-group">

                                    <button type="submit" class="btn btn-alt-primary" onclick="javascript:updatePassword()">修改</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>
        <!-- END Hero -->
    </div>
    <!-- END Page Content -->
@endsection

@section('js_after')
<script>
    function updateProfile() {
        $('#user_information').validate({
            submitHandler: function(form) {
                $.post(
                    "{{ route('profile.update') }}",
                    {
                        name: $('#name').val(),
                        email: $('#email').val(),
                        mobile: $('#mobile').val()
                    },
                    function (result) {

                    }
                ).fail(function (response) {});
            }
        });
    }

    function updatePassword() {
        $('#update_password').validate({
            rules: {
                old_password: {required: true},
                new_password: {minlength: 8},
                confirm_password: {minlength: 8, equalTo: '#new_password'}
            },
            submitHandler: function (form) {
                $.post(
                    "{{ route('profile.updatePassword') }}",
                    {
                        old_password: $('#old_password').val(),
                        new_password: $('#new_password').val(),
                        confirm_password: $('#confirm_password').val()
                    },
                    function (result) {

                    }
                );
            }
        });

    }
</script>
@endsection
