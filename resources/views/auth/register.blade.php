@extends('layouts.simple')

@section('content')
<!-- Page Content -->
<div class="bg-image" style="background-image: url('./media/photos/photo34@2x.jpg');">
    <div class="row mx-0 bg-black-op">
        <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
            <div class="p-30 invisible" data-toggle="appear">
                <p class="font-size-h3 font-w600 text-white">
                    Get Inspired and Create.
                </p>
                <p class="font-italic text-white-op">
                    Copyright &copy; <span class="js-year-copy"></span>
                </p>
            </div>
        </div>
        <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
            <div class="content content-full">
                <!-- Header -->
                <div class="px-30 py-10">
                    <a class="link-effect font-w700" href="index.html">
                        <i class="si si-trophy"></i>
                        <span class="font-size-xl text-primary-dark">泰慶興業</span><span class="font-size-xl">有限公司</span>
                    </a>
                    <h2 class="h5 font-w400 text-muted mb-0">新增用戶</h2>
                </div>
                <!-- END Header -->

                <!-- Sign In Form -->
                <form class="js-validation-signin px-30" action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="form-group row @error('Corporation') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                                <select name="corporation[]" id="corporation" class="js-select2 form-control" multiple>
                                    @foreach ($corporations as $corporation)
                                <option value="{{ $corporation->id}}">{{ $corporation->name }}</option>
                                    @endforeach
                                </select>
                                <label for="Corporation">{{ __('集團') }}</label>
                                @error('Corporation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('name') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="name" type="text" class="form-control " name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                <label for="name">{{ __('Name') }}</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('mobile') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="mobile" type="text" class="form-control " name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile">
                                <label for="mobile">{{ __('Mobile') }}</label>
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('email') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('position') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="position" type="text" class="form-control " name="position" value="{{ old('position') }}" required autocomplete="position">
                                <label for="position">{{ __('Position') }}</label>
                                @error('position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('username') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="username" type="text" class="form-control " name="username" value="{{ old('username') }}" required autocomplete="username">
                                <label for="username">{{ __('Username') }}</label>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('password') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input id="password" type="password" class="form-control " name="password" required autocomplete="current-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row @error('password-confirm') is-invalid @enderror">
                        <div class="col-12">
                            <div class="form-material floating">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-hero btn-alt-primary">
                            <i class="si si-login mr-10"></i> {{ __('Register') }}
                        </button>
                    </div>
                </form>
                <!-- END Sign In Form -->
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection

@section('css_after')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js_after')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>jQuery(function(){ Codebase.helpers('select2'); }); </script>
{{-- <script>
    $('#corporation').select2();
</script> --}}
@endsection
