
<div class="modal fade" id="modal-orderItem" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="block block-themed block-transparent mb-0">
        <div class="block-header bg-primary-dark">
          <h3 class="block-title">訂單項目 - <span class="modal-name"></span></h3>
          <div class="block-options">
            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
            </button>
          </div>
        </div>
        <form id="form-orderItem">
        <div class="block-content">
          <input type="hidden" id="orderItem-order_item_id" name="order_item_id" class="hidden" value=""/>
          <input type="hidden" id="orderItem-order_page_id" name="order_page_id" class="hidden" value=""/>
          <input type="hidden" id="orderItem-row_number" name="row_number" class="hidden" value=""/>

          <div class="form-row form-group required">
            <div class="col-sm-12">
              <label  class="control-label" for="name">品項</label>
              <div class="input-group">
                <span id="orderItem-rank" class="input-group-text">1</span>
                <input type="text" class="form-control" id="orderItem-name" name="name" placeholder="品項" required>
              </div>
            </div>
          </div>

          <div class="form-row form-group required">
            <div class="col-sm-6">
              <label for="progress" class="control-label">進度</label>
              <select name="progress" id="orderItem-progress" class="form-control" required>
                <option value="收到訂單">收到訂單</option>
                <option value="發包中">發包中</option>
                <option value="生產中">生產中</option>
                <option value="烤漆包裝">烤漆包裝</option>
                <option value="已出貨">已出貨</option>
                <option value="等排程">等排程</option>
                <option value="等出貨">等出貨</option>
                <option value="等SKIN板">等SKIN板</option>
                <option value="等客戶回復">等客戶回復</option>
                <option value="取消訂單">取消訂單</option>
              </select>
            </div>
            <div class="col-sm-6">
              <label for="name" class="control-label">出貨日</label>
              <input type="text" class="js-datepicker form-control" id="orderItem-due_date" name="due_date" placeholder="出貨日"  data-week-start='7' data-autoclose='true' data-today-highlight='true' data-date-format="yyyy-mm-dd" autocomplete="off" >
            </div>
          </div>


          <div class="form-row form-group required">
            <div class="col-sm-4">
              <label for="big_category" class="control-label">大類</label>
              <select name="big_category" id="orderItem-big_category" class="form-control" required>
                <option value=""> --- </option>
                <option value="BN">BN</option>
                <option value="BS">BS</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="middle_category" class="control-label">中類</label>
              <select name="middle_category" id="orderItem-middle_category" class="form-control" required>
                  <option value=""> --- </option>
                  <option class="frame hide" value="RK">RK</option>
                  <option class="frame hide" value="RF">RF</option>
                  <option class="door hide"  value="L8">L8</option>
                  <option class="door hide"  value="LK">LK</option>
                  <option class="door hide"  value="L7">L7</option>
                </select>
            </div>
            <div class="col-sm-4">
              <label for="window" class="control-label">上中下窗</label>
              <input type="text" class="form-control" id="orderItem-window" name="window" placeholder="上中下窗" required>
            </div>
          </div>

          <div class="form-row form-group required">
            <div class="col-sm-4">
              <label  class="control-label" for="width">寬度</label>
              <div class="input-group">
                <input type="number" class="form-control" id="orderItem-width" name="width" placeholder="寬度" required>
                <span class="input-group-text">mm</span>
              </div>
            </div>
            <div class="col-sm-4">
              <label for="up_window" class="control-label">上窗高度</label>
              <div class="input-group">
                <input type="number" class="form-control" id="orderItem-up_window" name="up_window" placeholder="上窗高度" required>
                <span class="input-group-text">mm</span>
              </div>
            </div>
            <div class="col-sm-4">
              <label for="total_height" class="control-label">總高度</label>
              <div class="input-group">
                <input type="number" class="form-control" id="orderItem-total_height" name="total_height" placeholder="總高度" required>
                <span class="input-group-text">mm</span>
              </div>
            </div>
          </div>

          <div class="form-row form-group required">
            <div class="col-sm-2">
              <label  class="control-label" for="wind">風壓</label>
              <input type="text" class="form-control" id="orderItem-wind" name="wind" placeholder="風壓" required>
            </div>
            <div class="col-sm-2">
              <label for="colour" class="control-label">色號</label>
              <input type="text" class="form-control" id="orderItem-colour" name="colour" placeholder="色號" required>
            </div>
            <div class="col-sm-2">
              <label  class="control-label" for="amount">數量</label>
              <input type="number" class="form-control" id="orderItem-amount" name="amount" placeholder="數量" required>
            </div>
            <div class="col-sm-2">
                <label  class="control-label" for="shipped">已出數量</label>
                <input type="number" class="form-control" id="orderItem-shipped" name="shipped" placeholder="已出貨數量" min="0" max="1000" required>
              </div>
            <div class="col-sm-4">
              <label for="price" class="control-label">單價</label>
              <div class="input-group">
                <div class="input-group-text">$</div>
                <input type="number" class="form-control" id="orderItem-price" name="price" placeholder="單價" required>
              </div>
            </div>
          </div>

          <div class="form-row form-group frame hide">
            <div class="col-sm-6">
              <label  class="control-label" for="threshold">門框種類</label>
              <input type="text" class="form-control" id="orderItem-threshold" name="threshold" placeholder="門框種類" value="">
            </div>
            <div class="col-sm-6">
              <label  class="control-label" for="depth">埋入深度</label>
              <input type="number" class="form-control" id="orderItem-depth" name="depth" placeholder="埋入深度" value="0">
            </div>
          </div>

          <div class="form-row form-group required">
            <div class="col-sm-6">
              <label  class="control-label" for="mixed_frame">混合框</label>
              <input type="text" class="form-control" id="orderItem-mixed_frame" name="mixed_frame" placeholder="混合框" required>
            </div>
            <div class="col-sm-6">
              <label for="box" class="control-label">BOX色號</label>
              <input type="text" class="form-control" id="orderItem-box" name="box" placeholder="BOX色號" required>
            </div>
          </div>

          <div class="form-row form-group door hide">
            <div class="col-sm-1"></div>
            <div class="col-sm-2">
              <label  class="control-label" for="l1">L1</label>
              <input type="number" class="form-control" id="orderItem-l1" name="l1" placeholder="L1" value="0">
            </div>
            <div class="col-sm-2">
              <label  class="control-label" for="l2">L2</label>
              <input type="number" class="form-control" id="orderItem-l2" name="l2" placeholder="L2" value="0">
            </div>
            <div class="col-sm-2">
              <label  class="control-label" for="l3">L3</label>
              <input type="number" class="form-control" id="orderItem-l3" name="l3" placeholder="L3" value="0">
            </div>
            <div class="col-sm-2">
              <label  class="control-label" for="l1">L4</label>
              <input type="number" class="form-control" id="orderItem-l4" name="l4" placeholder="L4" value="0">
            </div>
            <div class="col-sm-2">
              <label  class="control-label" for="l5">L5</label>
              <input type="number" class="form-control" id="orderItem-l5" name="l5" placeholder="L5" value="0">
            </div>
          </div>

          <div class="form-row form-group required">
            <div class="col-sm-6">
              <label  class="control-label" for="lock">鎖</label>
              <input type="text" class="form-control" id="orderItem-lock" name="lock" placeholder="鎖" required>
            </div>
            <div class="col-sm-6">
              <label for="lock_height" class="control-label">鎖高</label>
              <div class="input-group">
                <input type="number" class="form-control" id="orderItem-lock_height" name="lock_height" placeholder="鎖高" required>
                <span class="input-group-text">mm</span>
              </div>
            </div>
          </div>

          <div class="form-row form-group">
            <div class="col-sm-3">
              <label  class="control-label" for="f1">框一</label>
              <input type="text" class="form-control" id="orderItem-f1" name="f1" placeholder="框一" value="">
            </div>
            <div class="col-sm-3">
              <label  class="control-label" for="f2">框二</label>
              <input type="text" class="form-control" id="orderItem-f2" name="f2" placeholder="框二" value="">
            </div>
            <div class="col-sm-3">
              <label  class="control-label" for="f3">框三</label>
              <input type="text" class="form-control" id="orderItem-f3" name="f3" placeholder="框三" value="">
            </div>
            <div class="col-sm-3">
              <label  class="control-label" for="f4">框四</label>
              <input type="text" class="form-control" id="orderItem-f4" name="f4" placeholder="框四" value="">
            </div>
          </div>

          <div class="form-row form-group frame hide">
            <div class="col-sm-4">
              <label for="reinforce_iron" class="control-label">補強鐵</label>
              <select name="reinforce_iron" id="orderItem-reinforce_iron" class="form-control">
                <option value="" selected> 無 </option>
                <option value="一般">一般</option>
                <option value="翅膀">翅膀</option>
                <option value="特殊">特殊</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="airtight_strip" class="control-label">氣密條</label>
              <select name="airtight_strip" id="orderItem-airtight_strip" class="form-control">
                <option value="" selected> 無 </option>
                <option value="1611">1611</option>
                <option value="827F">827F</option>
                <option value="801">801</option>
                <option value="802">802</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="smoke_strip" class="control-label">遮煙條</label>
              <select name="smoke_strip" id="orderItem-smoke_strip" class="form-control">
                <option value="" selected> 無 </option>
                <option value="4cm">4cm</option>
                <option value="5cm">5cm</option>
                <option value="特殊">特殊</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" onclick="javascript:updateOrderItem();">確認</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
      </form>
    </div>
  </div>
</div>
