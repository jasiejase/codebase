@extends('layouts.backend')

@section('css_before')
<link href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<style>
  li[role=group] .select2-results__group {
    color: red;
    background-color: lightgray;
    font-weight: bold;
    font-size: 1.1em;
  }
  .handsontable .colored {
    background: #FCB515;
    color: #222;
    animation: blinker 1s linear;
  }
  @keyframes blinker {
    50% {
      opacity: 0.5;
    }
  }
  .hide{
      display:none !important;
  }
</style>
@endsection

@section('content')
  <!-- Page Content -->
  <div class="content">
      <h2 class="content-heading">訂單資料</h2>

      <!-- Begin Basic Customer Details -->
      <div class="block">
        <div class="row block-content block-content-full">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="company"><h4>選擇客戶:</h4></label>
              <select name="company" id="company" class="form-control">
                <option value=""> --- </option>
                @foreach ($companies as $corporation)
                  <optgroup label="{{$corporation->name}}">
                    @foreach ($corporation->companies as $company)
                      <option value="{{ $company->id }}">{{ $company->company }}</option>
                    @endforeach
                  </optgroup>
                @endforeach
              </select>
            </div>
            <div class="form-group pt-20">
              <label class="control-label" for="project"><h4>選擇建案</h4></label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button type="button" class="btn btn-primary" id="new-project" disabled="true" data-toggle="modal" data-target="#modal-project">
                    <i class="fa fa-plus"></i> 新增建案
                  </button>
                </div>
                <select name="project" id="project" class="form-control" disabled="true">
                  <option value=""> --- </option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-3 border border-dark" style="padding: 0px;">
            <div id="project-info" class="block block-themed">
              <div class="block-header bg-gd-sea">
                <h3 class="block-title">建案資料</h3>
              </div>
              <div class="block-content">
                <table class="table">
                  <thead></thead>
                  <tbody>
                    <tr><td>地址:</td><td></td></tr>
                    <tr><td>工地聯繫:</td><td></td></tr>
                    <tr><td>負責人:</td><td></td></tr>
                    <tr><td>備註:</td><td></td></tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-3 border border-dark" style="padding: 0px;">
            <div id="company-info" class="block block-themed">
              <div class="block-header bg-gd-sun">
                <h3 class="block-title">客戶資料</h3>
              </div>
              <div class="block-content">
                <table class="table">
                  <thead></thead>
                  <tbody>
                    <tr><td>統編:</td><td></td></tr>
                    <tr><td>地址:</td><td></td></tr>
                    <tr><td>電話:</td><td></td></tr>
                    <tr><td>傳真:</td><td></td></tr>
                    <tr><td>聯繫人:</td><td></td></tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Basic Customer Details -->

      <!-- Begin Order Page Section -->
      <div class="block" id="orderPage-section">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
          <li class="nav-item">
              <a class="nav-link active" href="#btabs-alt-static-order">訂單基本資料</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="#btabs-alt-static-door-frame">門樘數量對應表</a>
          </li>
          <li class="nav-item ml-auto">
              <button type="button" class="btn-block-option mt-2 mr-2" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="btabs-alt-static-order" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
                <div id="toolbar-orderPage">
                  <button id="new-orderPage" class="btn btn-primary mr-5 mb-5" onclick="javascript:showTableModal('orderPage', [], -1, $modalIds);" disabled="true">
                      <i class="fa fa-plus mr-5"></i>新增訂單
                  </button>
                </div>
                <table id="orderPage" class="table table-sm table-striped"></table>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="btabs-alt-static-door-frame" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Order Page Section -->

      <!-- Begin Order Items Section -->
      <div class="block" id="orderItem-section">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
          <li class="nav-item">
              <a class="nav-link active" href="#btabs-alt-static-order-item" id="order-item-details">訂單資料</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="#btabs-alt-static-order-item-translate">南亞訂單轉換</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#btabs-alt-static-order-page-note">訂單備註</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#btabs-alt-static-order-item-history">訂單更變紀錄</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#btabs-alt-static-order-item-shipped-history">出貨紀錄</a>
          </li>
          <li class="nav-item ml-auto">
              <button type="button" class="btn-block-option mt-2 mr-2" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="btabs-alt-static-order-item" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
                <div id="toolbar-orderItem">
                    <button id="new-orderItem" class="btn btn-primary mr-5 mb-5" disabled="true">
                        <i class="fa fa-plus mr-5"></i>新增項目
                    </button>
                  {{-- <button id="new-orderItem" class="btn btn-primary mr-5 mb-5" onclick="" disabled="true">
                      <i class="fa fa-plus mr-5"></i>新增項目
                  </button> --}}
                  {{-- <button id="save-orderItem" class="btn btn-primary mr-5 mb-5" onclick="" disabled="true">
                    <i class="fa fa-save mr-5"></i>Save訂單
                  </button> --}}
                </div>
                <table id="orderItem" class="table table-sm table-striped"></table>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="btabs-alt-static-order-item-translate" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
                <table id="orderItemTranslation" class="table table-sm table-striped"></table>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="btabs-alt-static-order-page-note" role="tabpanel">
            <form action="/OrderPageNote" id="newOrderPageForm" enctype=multipart/form-data>
                <div class="row block-content block-content-full">
                    <textarea name="note" id="newOrderPageNote-note" class="form-control"></textarea>
                    <input type="file" class="form-control" name="newOrderPageNoteFile[]" id="newOrderPageNoteFile" multiple>
                    <button type="submit" class="btn btn-primary">確認</button>
                </div>
            </form>
            <hr>
            <div id="orderPageNotes"></div>
          </div>
          <div class="tab-pane" id="btabs-alt-static-order-item-history" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
                <table id="orderItemHistory" class="table table-sm table-striped"></table>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="btabs-alt-static-order-item-shipped-history" role="tabpanel">
            <div class="row block-content block-content-full">
              <div class="col-12">
                <table id="orderItemShippedHistory" class="table table-sm table-striped"></table>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- End ORder Items Section -->
  </div>
  <!-- END Page Content -->

  <!-- Modal for new Project -->
@component('components.modal', ['formDisplay' => $projectModal])
  @slot('id') project @endslot
  @slot('title') 建案資料 @endslot

  @slot('footer')
      <button type="submit" class="btn btn-alt-success" onclick="javascript:editProject();">
          <i class="fa fa-check"></i>確認
      </button>
      <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
  @endslot
@endcomponent
  <!-- End Modal for new Project -->

    <!-- Modal for orderPage Detail -->
@component('components.modal', ['formDisplay' => $orderPageModal])
  @slot('id') orderPage @endslot
  @slot('title') 訂單基本資料 @endslot

  @slot('footer')
      <button type="submit" class="btn btn-alt-success" onclick="javascript:editOrderPage();">
          <i class="fa fa-check"></i>確認
      </button>
      <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
  @endslot
@endcomponent
  <!-- End Modal for orderPage detail -->

@include('project.orderItemModal');

@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('js/project.js') }}"></script>
<script src="{{ asset('js/orderPage.js') }}"></script>
<script src="{{ asset('js/orderItem.js') }}"></script>
<script src="{{ asset('js/orderPageNote.js') }}"></script>
<script>
  $('header, body').css('background-image','url("/img/75.jpg")');
  $('header').css('background-position', '-50px 0px');
</script>

@endsection
