<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <span class="text-dual-primary-dark">t</span><span class="text-primary">c</span>
                </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="link-effect font-w700" href="{{ route('home') }}">
                        <i class="si si-trophy text-primary"></i>
                        <span class="font-size-xl text-dual-primary-dark">泰慶興業</span><span class="font-size-xl text-primary">有限公司</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Sidebar Scrolling -->
        <div class="js-sidebar-scroll">
            <!-- Side User -->
            <div class="content-side content-side-full content-side-user px-10 align-parent">
                <!-- Visible only in mini mode -->
                <div class="sidebar-mini-visible-b align-v animated fadeIn">
                    <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                </div>
                <!-- END Visible only in mini mode -->

                <!-- Visible only in normal mode -->
                <div class="sidebar-mini-hidden-b text-center">
                    <a class="img-link" href="{{ route('profile.index') }}">
                        <img class="img-avatar" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                    </a>
                    <ul class="list-inline mt-10">
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark font-size-sm font-w600 text-uppercase" href="javascript:void(0)">{{ $name }}</a>
                        </li>
                        <li class="list-inline-item">
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="#">
                                <i class="si si-drop"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="si si-logout"></i>
                                </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
                <!-- END Visible only in normal mode -->
            </div>
            <!-- END Side User -->

            <!-- Side Navigation -->
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    @if(Auth::user()->id == 1)
                    <li>
                        <a href="{{ route('register') }}">
                            <i class='fa fa-plus mr-5'></i><span class='sidebar-mini-hide'>新增帳戶</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ request()->is('user') ? ' active' : '' }}" href="{{ route('user.index') }}">
                            <i class="fa fa-users""></i><span class="sidebar-mini-hide">使用者</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ request()->is('corporation') ? ' active' : '' }}" href="{{ route('corporation.index') }}">
                            <i class="fa fa-building""></i><span class="sidebar-mini-hide">集團</span>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a class="{{ request()->is('dashboard') ? ' active' : '' }}" href="{{ route('home') }}">
                            <i class="fa fa-tachometer""></i><span class="sidebar-mini-hide">看板</span>
                        </a>
                    </li>
                    <li class="nav-main-heading">
                        <span class="sidebar-mini-visible">CI</span><span class="sidebar-mini-hidden">Client Information</span>
                    </li>
                    <li>
                        <a class="{{ request()->is('company') ? ' active' : '' }}" href="{{ route('company.index') }}">
                            <i class="fa fa-address-card""></i><span class="sidebar-mini-hide">客戶資料</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ request()->is('project') ? ' active' : '' }}" href="{{ route('project.index') }}">
                            <i class="fa fa-truck"></i><span class="sidebar-mini-hide">訂單資料</span>
                        </a>
                    </li>
                    <li class="nav-main-heading">
                        <span class="sidebar-mini-visible">FD</span><span class="sidebar-mini-hidden">Finance Details</span>
                    </li>
                    <li>
                        <a class="{{ request()->is('supplier') ? ' active' : '' }}" href="{{ route('supplier.index') }}">
                            <i class="fa fa-child"></i><span class="sidebar-mini-hide">供應商</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ request()->is('material') ? ' active' : '' }}" href="{{ route('material.index') }}">
                            <i class="fa fa-cubes"></i><span class="sidebar-mini-hide">原物料</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ request()->is('shipment') ? ' active' : '' }}" href="{{ route('shipment.index') }}">
                            <i class="fa fa-money"></i><span class="sidebar-mini-hide">採購</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END Side Navigation -->
        </div>
        <!-- END Sidebar Scrolling -->
    </div>
    <!-- Sidebar Content -->
</nav>
