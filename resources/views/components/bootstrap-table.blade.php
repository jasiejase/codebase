<table id="{{ $tableId }}">
</table>

<div id="toolbar-{{ $tableId }}">
    {{ $toolbarContent }}
</div>

<script>
    var $table = $('#' + tableId);
    var $toolbar = '#toolbar-' + tableId;

    function initTable(tableId, headers, data) {
        var selections = [];

        $table.bootstrapTable('destroy').bootstrapTable({
            columns: headers,
            data: data,
            locale: 'zh-TW',
            theadClasses: 'thead-dark',
            exportDataType: 'all',
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            fixedColumns: true,
            fixedNumber: {{ $fixedColumn }},
            fixedRightNumber: {{ $fixedRightColumn }},
            toolbar: $toolbar,
            search: true,
            showSearchClearButton: true,
            showToggle: true,
            showFullscreen: true,
            showExport: true,
            showPrint: true,
            sortable: true,
            pagination: true,
            clickToSelect: true
        });

        $table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length) {
                selections = getIdSelections();
            }
        });
        
    }

    function getIdSelections(table) {
        return $.map(table.bootstrapTable('getSelections'), function (row) {
            return row.id
    });

    function getColumnSum(table, data) {
        
    }
</script>