<div class="modal fade" id="modal-{{ $id }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
              <div class="block-header bg-primary-dark">
                  <h3 class="block-title">{{ $title }} - <span class="modal-name"></span></h3>
                  <div class="block-options">
                      <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                          <i class="si si-close"></i>
                      </button>
                  </div>
              </div>
            <form id="form-{{ $id }}">
              <div class="block-content">
                @foreach ($formDisplay as $item)
                    @switch ($item['type'])
                        @case("hidden")
                            <input type="hidden" id="{{ $id }}-{{ $item['name'] }}" name="{{ $item['name'] }}" value="{{ $item['value'] ?? '' }}">
                            @break

                        @case("text")
                            <div class="form-group">
                                <label for="{{ $item['name'] }}">{{ $item['text'] }}</label>
                                <input type="text" class="form-control" id="{{ $id }}-{{ $item['name'] }}" name="{{ $item['name'] }}" placeholder="請輸入{{ $item['text'] }}" value="{{ $item['value'] ?? '' }}" {{ $item['validation'] ?? '' }} {{ $item['disabled'] ?? '' }}>
                            </div>
                            @break

                        @case("select")
                            <div class="form-group">
                                <label for="{{ $item['name'] }}">{{ $item['text'] }}</label>
                                <select class="form-control js-select2" name="{{ $item['name'] }}" id="{{ $id }}-{{ isset($item['id']) ? $item['id'] : $item['name'] }}" placeholder="請輸入{{ $item['text'] }}" {{ $item['validation'] ?? '' }} {{ $item['disabled'] ?? '' }} style="width:100%;">
                                    @foreach ($item['options'] as $text => $val)
                                        <option value="{{ $val }}">{{ $text }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @break
                        @case("textarea")
                            <div class="form-group">
                                <label for="{{ $item['name'] }}">{{ $item['text'] }}</label>
                                <textarea class="form-control" name="{{ $item['name'] }}" id="{{ $id }}-{{ isset($item['id']) ? $item['id'] : $item['name'] }}" rows="8" {{ $item['validation'] ?? '' }} {{ $item['disabled'] ?? '' }} ></textarea>
                            </div>
                            @break
                        @case("date")
                        <div class="form-group">
                            <label for="{{ $item['name'] }}">{{ $item['text'] }}</label>
                            <input type="text" class="js-datepicker form-control" id="{{ $id }}-{{ $item['name'] }}" name="{{ $item['name'] }}" placeholder="請輸入日期" value="{{ $item['value'] ?? '' }}" {{ $item['validation'] ?? '' }} {{ $item['disabled'] ?? '' }} data-week-start='7' data-autoclose='true' data-today-highlight='true' data-date-format="yyyy-mm-dd" autocomplete="off">
                        </div>
                    @endswitch
                @endforeach
              </div>
          </div>
          <div class="modal-footer">
              {{ $footer }}
          </div>
        </form>
      </div>
  </div>
</div>
