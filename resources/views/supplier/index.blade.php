@extends('layouts.backend')

@section('css_before')
@endsection


@section('content')
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">{{ $title }}</h2>
        <div class="block">
            <div id="toolbar-table">
                <button id="new-customer" class="btn btn-primary" onclick="javascript:showTableModal('table', [], -1, $modalIds);">
                    <i class="fa fa-plus mr-5"></i>新增供應商
                </button>
            </div>
            <div class="block-content block-content-full table-responsive">
                <table id="table" class="table table-sm table-striped"></table>
            </div>
        </div>
        <div class="block d-none">
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-alt-static-quote">歷史報價表</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-alt-static-shipment">歷史採購表</a>
                </li>
                <li class="nav-item ml-auto">
                    <button type="button" class="btn-block-option mt-2 mr-2" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="btabs-alt-static-quote" role="tabpanel">
                  <div class="row block-content block-content-full">
                    <div id="toolbar-material">
                        <h3></h3>
                    </div>
                    <div class="block-content block-content-full table-responsive">
                        <table id="material" class="table table-sm table-striped"></table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="btabs-alt-static-shipment" role="tabpanel">
                  <div class="row block-content block-content-full">
                    <div id="toolbar-shipment">
                        <h3></h3>
                    </div>
                    <div class="block-content block-content-full table-responsive">
                        <table id="shipment" class="table table-sm table-striped"></table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal For Table -->
    @component('components.modal', ['formDisplay' => $modal])
        @slot('id') table @endslot
        @slot('title') 供應商資料 @endslot

        @slot('footer')
            <button id="confirm_button" type="submit" class="btn btn-alt-success"
                    onclick="javascript:editBTableRow('table', $modalIds, '{{ route('supplier.create') }}', '{{ route('supplier.update') }}')">
                <i class="fa fa-check"></i>確認
            </button>
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
        @endslot
    @endcomponent
    <!-- End Modal For Table -->

    <!-- Modal for sellers -->
    <div class="modal fade" id="modal-seller" tabindex="-2" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">供應商聯絡人 - <span class="modal-name"></span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>

                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-4">
                                <ul id="seller_list" class="list-group">
                                    <a href="#"><li class="list-group-item active">新增聯絡人</li></a>
                                    <a href="#"><li class="list-group-item">Jason</li></a>
                                </ul>
                            </div>

                            <div class="col-md-8">
                                <form id="form-seller">
                                    <input type="hidden" id="seller_id" name="seller_id" value="">
                                    <input type="hidden" id="supplier_id" name="supplier_id" value="">
                                    <div class="form-group">
                                        <label for="name">姓名</label>
                                        <input type="text" class="form-control" id="name" name="name" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="position">職位</label>
                                        <input type="text" class="form-control" id="position" name="position">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">電話 (含分機)</label>
                                        <input type="text" class="form-control" id="phone" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">手機</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="note">備註</label>
                                        <textarea class="form-control" name="note" id="note" rows="8"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button id="confirm_seller" type="submit" class="btn btn-alt-success"
                                                onclick="javascript:updateSeller();">
                                            <i class="fa fa-check"></i>確認
                                        </button>
                                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

    <!-- End Modal for sellers -->
@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify\bootstrap-notify.min.js') }}"></script>
<script>
    let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
    let $modalIds = {}; // cannot change name coz used in window.operateEvents in bootstrap-table
    $modalIds['id'] = new Array('index', 'id', 'supplier', 'gui', 'address', 'phone', 'phone2', 'fax', 'website', 'email','mat_id');
    $modalIds['disabled'] = new Array('gui');
    $modalIds['multiple'] = {'mat_id' : 'mat'};
    $modalIds['name'] = 'supplier';
    $modalIds['newName'] = '新供應商';
    $modalIds['rowButtons'] = ['viewMaterial', 'edit', 'seller'];
    $modalIds['main'] = 'supplier';

    createBTable('table', "{{ route('supplier.show') }}", sRow, false);



    // Javascript for contact modal
    function selectSeller(id, index) {
        $('.list-group-item').removeClass('active');
        $('#seller_list a:nth-child(' + index + ') li').addClass('active');
        $('#seller_id').val(id);

        if (id == 0) {
            $('#name').val('');
            $('#position').val('');
            $('#phone').val('');
            $('#mobile').val('');
            $('#email').val('');
            $('#note').val('');
            return false;
        }

        var url  = "{{ route('seller.show') }}";
        var data = { 'id' : id }
        $.post(url, data, function (result) {
            $('#name').val(result.seller.name);
            $('#position').val(result.seller.position);
            $('#phone').val(result.seller.phone);
            $('#mobile').val(result.seller.mobile);
            $('#email').val(result.seller.email);
            $('#note').val(result.seller.note);
        });
    };

    function updateSeller() {
        $('#form-seller').validate({
            submitHandler: function () {
                var url  = "{{ route('seller.edit') }}";
                var data = {
                        id : $('#seller_id').val(),
                        supplier_id: $('#supplier_id').val(),
                        name: $('#name').val(),
                        position: $('#position').val(),
                        phone: $('#phone').val(),
                        mobile: $('#mobile').val(),
                        email: $('#email').val(),
                        note: $('#note').val()
                }

                $.post(url, data, function (result) {
                    var message = "";
                    var icon = "";

                    if (data.id == "0") {
                        var index = $('#seller_list a').length + 1;
                        $('#seller_list').append('<a href="javascript:selectseller(' + result.id + ', ' + index  + ')"><li class="list-group-item"><i class="fa fa-user" /> ' + data.name + '</li></a>');
                        selectseller(result.id, index);
                        message = "成功增加 " + $('.modal-name').html() + " 聯絡人 " + data.name;
                        icon = 'fa fa-user-plus';
                    } else {
                        message = data.name + "的資料修改成功";
                        icon = 'fa fa-user-edit';
                    }

                    if (result.changed) {
                        notify(message, icon, 'success', '#modal-seller');
                    }
                });
            }
        });
    }

    function viewMaterial(row) {
        var $table = $('#material');
        $('#toolbar-material h3').html("供應商:" + row.supplier);
        createBTable('material', "{{ route('supplier.viewMaterial') }}", sRow, false, {"id": row.id});
        $('.block').removeClass('d-none');
    }

    function viewShipment(row) {
        $('#toolbar-shipment h3').html("供應商:" + row.supplier);
        createBTable('shipment', "{{ route('supplier.viewShipment') }}", sRow, false, {"id": row.id});
        $('.block').removeClass('d-none');
    }

    function availableFormatter(value, row) {
        if (value == "1") {
            return '<span class="text text-success"><i class="fa fa-check-circle"></i> YES</span>';
        }
        return '<span class="text text-danger"><i class="fa fa-times-circle"></i> NO</span>';
    }

    function priceFormatter(value, row) {
        if (row.available) {
            return "$" + value;
        }
        return "<span class='text text-danger'><del>$" + value + "</del></span>";
    }

    function approvedFormatter(value, row) {
        if (value) {
            return '<span class="text text-success"><i class="fa fa-thumbs-up"></i> OK</span>';
        }
        return '<span class="text text-danger"><i class="fa fa-times"></i> 未核准</span>';
    }
    function receivedAtFormatter(value, row) {
        if (!value) {
            return '<span class="text text-warning"><i class="fa fa-car"></i> 未收到</span>';
        }
        return value;
    }

    $('header, body').css('background-image','url("/img/57.jpg")');
    $('header').css('background-position', '-50px 0px');
</script>
@endsection
