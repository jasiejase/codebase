@extends('layouts.backend')

@section('css_before')
<style>
.thead-blue {
    color: #ddd;
    background-color: #7abaff;
}

.thead-yellow {
    color: #636363;
    background-color: #FAFA33;
}

.thead-pink {
    color: #f1f1f1;
    background-color: #E16389;
}

.thead-purple {
    color: #262626;
    background-color: #D6CDEA;
}

.thead-brown {
    color: #f1f1f1;
    background-color: #BFAC98;
}

.thead-green {
    color: #131313;
    background-color: #AAF0D1;
}
</style>

@endsection


@section('content')
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">{{ $title }}</h2>
    <div class="block">
        <div id="toolbar-table">
            <button id="new-customer" class="btn btn-primary" onclick="javascript:showTableModal('table', [], -1, $modalIds);">
                <i class="fa fa-plus mr-5"></i>新增報價單
            </button>
        </div>
        <div class="block-content block-content-full table-responsive">
            <table id="table" class="table table-sm table-striped"></table>
        </div>
    </div>
    {{-- <div class="block d-none">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-alt-static-quote">歷史報價表</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-alt-static-shipment">歷史採購表</a>
            </li>
            <li class="nav-item ml-auto">
                <button type="button" class="btn-block-option mt-2 mr-2" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="btabs-alt-static-quote" role="tabpanel">
                <div class="row block-content block-content-full">
                <div id="toolbar-material">
                    <h3></h3>
                </div>
                <div class="block-content block-content-full table-responsive">
                    <table id="material" class="table table-sm table-striped"></table>
                </div>
                </div>
            </div>
            <div class="tab-pane" id="btabs-alt-static-shipment" role="tabpanel">
                <div class="row block-content block-content-full">
                <div id="toolbar-shipment">
                    <h3></h3>
                </div>
                <div class="block-content block-content-full table-responsive">
                    <table id="shipment" class="table table-sm table-striped"></table>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div> --}}
</div>
<!-- END Page Content -->
@endsection


@section('js_after')
<script>
    let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
    let $modalIds = {}; // cannot change name coz used in window.operateEvents in bootstrap-table
    $modalIds['id'] = new Array('index', 'id', 'supplier', 'gui', 'address', 'phone', 'phone2', 'fax', 'website', 'email','mat_id');
    $modalIds['disabled'] = new Array('gui');
    $modalIds['multiple'] = {'mat_id' : 'mat'};
    $modalIds['name'] = 'quote';
    $modalIds['newName'] = '新估價單';
    $modalIds['rowButtons'] = ['edit'];
    $modalIds['main'] = 'quote';

    createBTable('table', "{{ route('quote.show') }}", sRow, false);


    $('header, body').css('background-image','url("/img/57.jpg")');
    $('header').css('background-position', '-50px 0px');
</script>
@endsection