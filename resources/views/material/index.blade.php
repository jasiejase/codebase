@extends('layouts.backend')

@section('css_before')
@endsection


@section('content')
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">{{ $title }}</h2>
        <div class="block">
            <div id="toolbar-table">
                <button id="new-customer" class="btn btn-primary" onclick="javascript:showTableModal('table', [], -1, $modalIds);">
                    <i class="fa fa-plus mr-5"></i>新增原物料
                </button>
            </div>
            <div class="block-content block-content-full table-responsive">
                <table id="table" class="table table-sm table-striped"></table>
            </div>
        </div>
        <div class="block d-none">
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-alt-static-quote">歷史報價表</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-alt-static-shipment">歷史採購表</a>
                </li>
                <li class="nav-item ml-auto">
                    <button type="button" class="btn-block-option mt-2 mr-2" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="btabs-alt-static-quote" role="tabpanel">
                  <div class="row block-content block-content-full">
                    <div id="toolbar-material">
                        <h3></h3>
                    </div>
                    <div class="block-content block-content-full table-responsive">
                        <table id="material" class="table table-sm table-striped"></table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="btabs-alt-static-shipment" role="tabpanel">
                  <div class="row block-content block-content-full">
                    <div id="toolbar-shipment">
                        <h3></h3>
                    </div>
                    <div class="block-content block-content-full table-responsive">
                        <table id="shipment" class="table table-sm table-striped"></table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal For Table -->
    @component('components.modal', ['formDisplay' => $modal])
        @slot('id') table @endslot
        @slot('title') 供應商資料 @endslot

        @slot('footer')
            <button id="confirm_button" type="submit" class="btn btn-alt-success"
                    onclick="javascript:editBTableRow('table', $modalIds, '{{ route('material.create') }}', '{{ route('material.update') }}')">
                <i class="fa fa-check"></i>確認
            </button>
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
        @endslot
    @endcomponent
    <!-- End Modal For Table -->

@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify\bootstrap-notify.min.js') }}"></script>
<script>
    let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
    let $modalIds = {}; // cannot change name coz used in window.operateEvents in bootstrap-table
    $modalIds['id'] = new Array('index', 'id', 'category', 'name', 'size', 'unit', 'undersupply', 'sup_id', 'note');
    $modalIds['disabled'] = new Array();
    $modalIds['multiple'] = {'sup_id' : 'sup'};
    $modalIds['name'] = 'material';
    $modalIds['newName'] = '新供應商';
    $modalIds['rowButtons'] = ['viewMaterial', 'edit'];
    $modalIds['main'] = 'material';

    createBTable('table', "{{ route('material.show') }}", sRow, false);

    // Javascript for contact modal

    function viewMaterial(row) {
        var $table = $('#material');
        $('#toolbar-material h3').html("原物料: " + row.name);
        createBTable('material', "{{ route('material.viewMaterial') }}", sRow, false, {"id": row.id});
        $('.block').removeClass('d-none');
    }

    function viewShipment(row) {
        $('#toolbar-shipment h3').html("原物料: " + row.name);
        createBTable('shipment', "{{ route('material.viewShipment') }}", sRow, false, {"id": row.id});
        $('.block').removeClass('d-none');
    }

    function availableFormatter(value, row) {
        if (value == "1") {
            return '<span class="text text-success"><i class="fa fa-check-circle"></i> YES</span>';
        }
        return '<span class="text text-danger"><i class="fa fa-times-circle"></i> NO</span>';
    }

    function priceFormatter(value, row) {
        if (row.available) {
            return "$" + value;
        }
        return "<span class='text text-danger'><del>$" + value + "</del></span>";
    }

    function approvedFormatter(value, row) {
        if (value) {
            return '<span class="text text-success"><i class="fa fa-thumbs-up"></i> OK</span>';
        }
        return '<span class="text text-danger"><i class="fa fa-times"></i> 未核准</span>';
    }
    function receivedAtFormatter(value, row) {
        if (!value) {
            return '<span class="text text-warning"><i class="fa fa-car"></i> 未收到</span>';
        }
        return value;
    }

    $('header, body').css('background-image','url("/img/57.jpg")');
    $('header').css('background-position', '-50px 0px');
</script>
@endsection
