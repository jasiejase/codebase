@extends('layouts.backend')

@section('css_before')
@endsection


@section('content')
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">使用者資料</h2>
        <div class="block">
            <div class="block-content block-content-full table-responsive">
                <table id="table" class="table table-sm table-striped"></table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal For Table -->
    @component('components.modal', ['formDisplay' => $modal])
        @slot('id') table @endslot
        @slot('title')
            用戶資料 - <span id="modal-name"></span>
        @endslot

        @slot('footer')
            <button id="confirm_button" type="submit"" class="btn btn-alt-success"
            onclick="javascript:editBTableRow('table', $modalIds, '', '{{ route('user.update') }}')">
                <i class="fa fa-check"></i>確認
            </button>
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
        @endslot
    @endcomponent
    <!-- End Modal For Table -->
@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify\bootstrap-notify.min.js') }}"></script>
<script>
    let sRow = {};
    let $modalIds = {};
    $modalIds['id'] = new Array('index', 'name', 'id', 'mobile', 'email', 'position', 'is_active', 'corp_id');
    $modalIds['disabled'] = new Array('mobile', 'email');
    $modalIds['multiple'] = {'corp_id' : 'corp'};
    $modalIds['main'] = 'name';

    createBTable('table', "{{ route('user.show') }}", sRow, false);

    var buttonOperation = {
        'click .edit': function (el, id, row, index) {
            showTableModal('table', row, index, $modalIds);
        }
    }

    function buttonColumn() {
        return '<a class="edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>';
    }

    function customFormatter (value, row) {
        if (row.is_active != "活躍") {
            return '<span class="text-danger">' + value + '</span>';
        }
        return value;
    }
    $('header, body').css('background-image','url("/img/73.jpg")');
    $('header').css('background-position', '-50px 0px');


</script>
@endsection
