@extends('layouts.backend')

@section('css_before')
@endsection


@section('content')
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">集團相關資料</h2>
        <div class="block">
            <div id="toolbar-table"></div>
            <div class="block-content block-content-full table-responsive">
                <table id="table" class="table table-sm table-striped"></table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal For Table -->
    @component('components.modal', ['formDisplay' => $modal])
        @slot('id') table @endslot
        @slot('title') 集團資料 @endslot

        @slot('footer')
            <button id="confirm_button"
                    type="button"
                    class="btn btn-alt-success"
                    onclick="tableModal.update(myTable, '', '{{ route('corporation.update') }}')">
                <i class="fa fa-check"></i>確認
            </button>
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">關閉</button>
        @endslot
    @endcomponent
    <!-- End Modal For Table -->
@endsection


@section('js_after')
<script src="{{ asset('js/plugins/bootstrap-notify\bootstrap-notify.min.js') }}"></script>
<script>
    // let sRow = {}; // must have to get bootstrap table working, but one per bootstrap table
    let $modalIds = {};
    $modalIds['id'] = new Array('index', 'id', 'name', 'company_ids', 'user_ids');
    $modalIds['disabled'] = new Array('corporation');
    $modalIds['multiple'] = {'company_ids' : 'company', 'user_ids' : 'user'};
    $modalIds['name'] = 'corporation';
    $modalIds['newName'] = '新集團';
    $modalIds['main'] = 'name';

    // createBTable('table', "{{ route('corporation.show') }}", sRow, false);

    // var buttonOperation = {
    //     'click .edit': function (el, id, row, index) {
    //         showTableModal('table', row, index, $modalIds);
    //     }
    // }

    // function buttonColumn() {
    //     return '<a class="edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>';
    // }
    let tableId = 'table';
    let tableModal = new TableModal(tableId, $modalIds);

    const url = "{{ route('corporation.show') }}";
    let data = {};
    let sRow = {};
    let button = [
        {
            name: 'edit',
            icon: 'edit',
            callback:  function (el, id, row, index) {
                // showTableModal(tableId, row, index, $modalIds);
                tableModal.show(row, index);
            }
        }
    ];

    let myTable = new BTableWrapper('table', url, data, button, sRow);
    myTable.addUserButtons();
    myTable.createTable();

    $('header, body').css('background-image','url("/img/8764.jpg")');
    $('header').css('background-position', '-50px 0px');
</script>
@endsection
